package com.devteach.devshaker.slides;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.devteach.devshaker.R;

/**
 * Created by miguelpereiralopez on 25/7/16.
 */
public class SecondSlide extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tutorial2, container, false);
        return v;
    }
}
