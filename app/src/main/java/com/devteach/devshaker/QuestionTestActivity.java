package com.devteach.devshaker;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.devteach.devshaker.model.Category;
import com.devteach.devshaker.model.DevShackerDbHelper;
import com.devteach.devshaker.model.Level;
import com.devteach.devshaker.model.Question;
import com.devteach.devshaker.model.Subcategory;
import com.devteach.devshaker.model.User;
import com.devteach.devshaker.utils.MenuActivityase;
import com.devteach.devshaker.utils.Utils;
import com.liferay.mobile.android.auth.basic.BasicAuthentication;
import com.liferay.mobile.android.callback.Callback;
import com.liferay.mobile.android.callback.typed.JSONArrayCallback;
import com.liferay.mobile.android.service.Session;
import com.liferay.mobile.android.service.SessionImpl;
import com.liferay.mobile.android.v7.api.ApiService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;



public class QuestionTestActivity extends MenuActivityase {


    private final String TAG = "QuestionTestActivity";
    private static Level levelSelected;
    private static long subcategoryId;
    private static Activity act;

    private static ProgressDialog progress;
    private static ProgressBar mProgress;

    private FrameLayout questionLayout;
    private Subcategory subcategory;

    private static Question actualQuestion;

    private int index;

    private static ImageView imageQuestion;

    private static LinkedList<Question> questions = new LinkedList<Question>();

    private static Button bA1;
    private static Button bA2;
    private static Button bA3;
    private static Button bA4;
    private static Button buttonNextQuestion;
    private static boolean isfromtutorail;

    private List<Boolean> listAnswerStatus = new LinkedList<Boolean>();

    private static DevShackerDbHelper dbhelper;
    private static RelativeLayout layoutToListenSwipe;
    private static JSONArray responses;
    private static String tokenData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            isfromtutorail = extras.getBoolean("IS_FROM_TUTORIAL");
        }
        //setContentView(R.layout.activity_question_test);
        responses = new JSONArray();
        tokenData = this.getSharedPreferences("com.devteach.devshaker", MODE_PRIVATE).getString("tokenData","");

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_question_test, contentFrameLayout);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        act = this;
        fab.setVisibility(View.GONE);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(null);
        mToolbar.setNavigationOnClickListener(null);

        layoutToListenSwipe = (RelativeLayout) findViewById(R.id.layoutToSwipe);

        Log.d("layoutToListenSwipe",layoutToListenSwipe.getLayoutParams().width+"");
        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();

        int height = display.getHeight();
        Log.d("display",width+"");

            this.index = 0;

        if (isfromtutorail) {
            //this.getActionBar()
            MenuActivityase.setActivityTitlebyResource(R.string.title_activity_tutorial);

            questionLayout = (FrameLayout) findViewById(R.id.questionLayout);
            mProgress = (ProgressBar) findViewById(R.id.progressBar);
            imageQuestion = (ImageView) findViewById(R.id.imageQuestion);
            questions = Utils.QUESTION_LIST_TUTORIAL;
            setQuestionInfo(questions.getFirst(), index, this);
        } else {
            questionLayout = (FrameLayout) findViewById(R.id.questionLayout);
            mProgress = (ProgressBar) findViewById(R.id.progressBar);
            imageQuestion = (ImageView) findViewById(R.id.imageQuestion);
            //Not from tutorial --> Could be new or loaded from DDBB
            dbhelper = DevShackerDbHelper.getInstance(this);
            levelSelected = (Level) getIntent().getSerializableExtra("Level");
            subcategory = (Subcategory) getIntent().getSerializableExtra("Subcategory");

            String categoryName= Category.getCategoryFromDDBB(dbhelper.getReadableDatabase(),subcategory.getCategory()).getName();
            MenuActivityase.setToolbarColor(categoryName);
            MenuActivityase.setActivityTitlebyText(subcategory.getName()+" "+levelSelected.getName());

            subcategoryId = subcategory.getId();
            User user = User.getUserFromDDBB(dbhelper.getReadableDatabase());
            int totalLevels = extras.getInt("totalLevels");
            Log.d(TAG, String.valueOf(subcategory.getId()) + " " + String.valueOf(levelSelected) + " " + String.valueOf(totalLevels));
            Log.d(TAG, "LevelSelected id: " + levelSelected.getID());

            String selectQuery = "SELECT * FROM " + Question.QuestionEntry.TABLE_NAME + " WHERE " + Question.QuestionEntry.COLUMN_NAME_SUBCATEGORY_ID + "=" + levelSelected.getSubCategory()
                    + " AND " + Question.QuestionEntry.COLUMN_NAME_DIFFICULTY + "= " + levelSelected.getID() + ";";

            Cursor cursor = dbhelper.getReadableDatabase().rawQuery(selectQuery, null);
            System.out.println(cursor.getCount());
            if (cursor.getCount() == 0) {
                progress = ProgressDialog.show(this, "Downloading",
                        "Downloading questions of test from Server, please wait", true);

                Log.d(TAG, "No questions for level, need to download");


                Session session = new SessionImpl(Utils.URL_BASE,new BasicAuthentication(user.getEmailAddress(),user.getPassword()));
                ApiService devshakerService = new ApiService(session);
                Callback callback = new JSONArrayCallback() {
                    @Override
                    public void onFailure(Exception exception) {
                        Log.d("QUESTION -->KO",exception.toString());
                        progress.dismiss();

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        Log.d("QUESTION -->OK","Recibvidas "+result.length());
                        Question question = null;

                        JSONObject obj = null;
                        for(int i = 0;i<result.length();i++){
                            try {
                                obj = result.getJSONObject(i);
                                long questionId= obj.getLong("questionId");
                                long subcatId= obj.getLong("subcategoryId");
                                String textQuestion = obj.getString("questionText");
                                String imageTextQuestion = null;
                                String an0 = obj.getString("answer0Text");
                                String an1 = obj.getString("answer1Text");
                                String an2 = obj.getString("answer2Text");
                                String an3 = obj.getString("answer3Text");
                                String correctAnswers = obj.getString("correctAnswers");
                                long level = obj.getLong("levelId");

                                question = new Question(questionId,subcatId,textQuestion,imageTextQuestion,an0,an1,an2,an3,correctAnswers,level);
                                Question.saveQuestionToDDBB(question,dbhelper.getWritableDatabase());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        questions = Question.getListQuestionBySubAndLevelFromDB(subcategoryId,levelSelected.getID(), levelSelected.getNumberOfQuestions(), dbhelper.getWritableDatabase());
                        setQuestionInfo(questions.getFirst(), index, act);
                        progress.dismiss();

                    }
                };

                session.setCallback(callback);

                try {
                    devshakerService.getTestQuestionOfLevel(levelSelected.getID());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Log.d(TAG, "There are questions on DDBB only to charge it");
                Log.d(TAG, subcategoryId + " " + levelSelected + " " + levelSelected.getNumberOfQuestions());
                questions = Question.getListQuestionBySubAndLevelFromDB(subcategoryId,levelSelected.getID(), levelSelected.getNumberOfQuestions(), dbhelper.getReadableDatabase());
                setQuestionInfo(questions.getFirst(), index, this);
            }
        }
    }

    public static void setQuestionInfo(Question q, int index, Activity a) {
        actualQuestion = q;
        TextView questionText  = (TextView) a.findViewById(R.id.txtQuestion);
        questionText.setMovementMethod(new ScrollingMovementMethod());
        Log.d("WIDTH QUESTIONTEXT",questionText.getLayoutParams().width+"");

        Log.d("QuestionTestActivity", "RespuestaCorrecta: " + actualQuestion.getRightAnswers());
        buttonNextQuestion = (Button) a.findViewById(R.id.buttonNextQuestion);

        bA1 = (Button) a.findViewById(R.id.buttonA1);
        bA2 = (Button) a.findViewById(R.id.buttonA2);
        bA3 = (Button) a.findViewById(R.id.buttonA3);
        bA4 = (Button) a.findViewById(R.id.buttonA4);
        String rawQuestionText = q.getTextQuestion();

        if(!isfromtutorail) {
            try {

                InputStream in = new ByteArrayInputStream(rawQuestionText.getBytes("UTF-8"));
                rawQuestionText = parse(in);
            }catch (Exception e){
                e.printStackTrace();
            }

        }

        System.out.println("rawQuestionText");
        System.out.println(rawQuestionText);

        questionText.setText(Html.fromHtml(rawQuestionText));

        if (q.getImageQuestion() != null && q.getImageQuestion().trim().length() != 0) {
            Uri imageUri = null;
            if (isfromtutorail) {
                imageUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                        a.getResources().getResourcePackageName(R.drawable.q3img) + '/' +
                        a.getResources().getResourceTypeName(R.drawable.q3img) + '/' +
                        a.getResources().getResourceEntryName(R.drawable.q3img));
            } else {

                Log.d("ImageURI", "ImageURI: " + q.getImageQuestion());

                imageUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                        a.getResources().getResourcePackageName(R.drawable.q3img) + '/' +
                        a.getResources().getResourceTypeName(R.drawable.q3img) + '/' +
                        a.getResources().getResourceEntryName(R.drawable.q3img));
            }
            System.out.println(imageUri.toString());
            imageQuestion.setImageURI(imageUri);
            imageQuestion.setVisibility(View.VISIBLE);
            questionText.setGravity(Gravity.NO_GRAVITY);
        } else {
            imageQuestion.setVisibility(View.INVISIBLE);
            questionText.setGravity(Gravity.CENTER);
            questionText.setGravity(Gravity.CENTER_VERTICAL);

        }

        bA1.setText(Html.fromHtml(q.getAnswer1()));
        bA2.setText(Html.fromHtml(q.getAnswer2()));
        bA3.setText(Html.fromHtml(q.getAnswer3()));
        bA4.setText(Html.fromHtml(q.getAnswer4()));

        mProgress.setProgress((index * 100) / questions.size());

        bA1.setActivated(false);
        bA1.setBackgroundResource(R.drawable.background);

        bA2.setActivated(false);
        bA2.setBackgroundResource(R.drawable.background);

        bA3.setActivated(false);
        bA3.setBackgroundResource(R.drawable.background);

        bA4.setActivated(false);
        bA4.setBackgroundResource(R.drawable.background);

        checkNextButton();
    }


    public static String parse(InputStream is) {
        String text = new String(),returnValue = new String();
        XmlPullParserFactory factory = null;
        XmlPullParser parser = null;
        try {
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            parser = factory.newPullParser();

            parser.setInput(is, null);

            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagname = parser.getName();

                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagname.equalsIgnoreCase("QuestionText")) {
                            // create a new instance of employee
                            Log.d("QuestionTestActivity","Question found in HTML");
                        }
                        break;

                    case XmlPullParser.TEXT:
                        Log.d("QuestionTestActivity",parser.getText());
                        text = parser.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        if (tagname.equalsIgnoreCase("QuestionText")) {
                            // add employee object to list
                            returnValue = text;
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return returnValue;
    }


    public void setFirstCorrect(View view) {

        if (bA1.isActivated()) {
            bA1.setActivated(false);
            bA1.setBackgroundResource(R.drawable.background);
        } else {
            bA1.setActivated(true);
            bA1.setBackgroundResource(R.drawable.selected_answer_bgn);
        }
        checkNextButton();
    }

    public void setSecondCorrect(View view) {
        if (bA2.isActivated()) {
            bA2.setActivated(false);
            bA2.setBackgroundResource(R.drawable.background);
        } else {
            bA2.setActivated(true);
            bA2.setBackgroundResource(R.drawable.selected_answer_bgn);
        }
        checkNextButton();
    }

    public void setThirdCorrect(View view) {
        if (bA3.isActivated()) {
            bA3.setActivated(false);
            bA3.setBackgroundResource(R.drawable.background);
        } else {
            bA3.setActivated(true);
            bA3.setBackgroundResource(R.drawable.selected_answer_bgn);
        }
        checkNextButton();
    }

    public void setFourthCorrect(View view) {
        if (bA4.isActivated()) {
            bA4.setActivated(false);
            bA4.setBackgroundResource(R.drawable.background);
        } else {
            bA4.setActivated(true);
            bA4.setBackgroundResource(R.drawable.selected_answer_bgn);
        }
        checkNextButton();
    }


    private static void checkNextButton(){
        if (!bA1.isActivated() && !bA2.isActivated() && !bA3.isActivated() && !bA4.isActivated()) {
            Log.i("No response", "No hay respuesta boton bloqueado");
            buttonNextQuestion.setEnabled(false);
            buttonNextQuestion.setAlpha(0.5f);
        }else{
            buttonNextQuestion.setEnabled(true);
            buttonNextQuestion.setAlpha(1);

        }
    }


    public void correctQuestion(View view) {
        Log.i("", "Corregimos");

        String[] correctAnswers;
        if (actualQuestion.getRightAnswers().contains("#")) {
            correctAnswers = actualQuestion.getRightAnswers().split("#");
        } else {
            correctAnswers = new String[1];
            correctAnswers[0] = actualQuestion.getRightAnswers();
        }

        List<String> selectedAnswers = new LinkedList<String>();
        String responseTosend = new String();
        //int counter = correctAnswers.length;
        String correctAnswersToSend = new String();

        if (bA1.isActivated()) {
            selectedAnswers.add("1");
            correctAnswersToSend += "1#";
        }

        if (bA2.isActivated()) {
            selectedAnswers.add("2");
            correctAnswersToSend += "2#";

        }

        if (bA3.isActivated()) {
            selectedAnswers.add("3");
            correctAnswersToSend += "3#";

        }

        if (bA4.isActivated()) {
            selectedAnswers.add("4");
            correctAnswersToSend += "4#";
        }

        correctAnswersToSend = correctAnswersToSend.substring(0, correctAnswersToSend.length() - 1);
        JSONObject obj = new JSONObject();
        try {
            obj.put("id", actualQuestion.getID());
            obj.put("response", correctAnswersToSend);
            obj.put("tokenData", tokenData);
            responses.put(obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        //Now --> Check it
        boolean istestFinal = false;
        System.out.println(selectedAnswers.size() + " " + correctAnswers.length);
        if (selectedAnswers.size() == correctAnswers.length) {
            int counter = 0;
            for (int i = 0; i < correctAnswers.length; i++) {
                if (selectedAnswers.contains(correctAnswers[i])) {
                    counter++;
                }
            }

            if (counter == correctAnswers.length) {
                //Todas respuestas correctas
                if (index + 1 < questions.size()) {
                    listAnswerStatus.add(index, true);
                    index = index + 1;
                    actualQuestion = questions.get(index);
                    setQuestionInfo(actualQuestion, index, act);
                } else {
                    istestFinal = true;
                    listAnswerStatus.add(index, true);

                }

            } else {
                Log.i("Response error", "La respuesta a la pregunta no es correcta");
                if (index + 1 < questions.size()) {
                    listAnswerStatus.add(index, false);
                    index = index + 1;
                    actualQuestion = questions.get(index);
                    setQuestionInfo(actualQuestion, index, act);
                } else {
                    istestFinal = true;
                    listAnswerStatus.add(index, false);

                }
            }
        } else {
            Log.i("Response error", "La respuesta a la pregunta no es correcta");
            if (index + 1 < questions.size()) {
                listAnswerStatus.add(index, false);
                index = index + 1;
                actualQuestion = questions.get(index);
                setQuestionInfo(actualQuestion, index, act);
            } else {
                istestFinal = true;
                listAnswerStatus.add(index, false);

            }

        }


        if (istestFinal) {
            Log.i("FINAL DE TEST", "FINAL DEL TEST");
            Iterator<Boolean> iterator = listAnswerStatus.iterator();
            boolean listArray[] = new boolean[listAnswerStatus.size()];
            int i = 0;
            while (iterator.hasNext()) {
                boolean value = iterator.next();
                listArray[i] = value;
                Log.i("Respuesta Correcta?", new Boolean(value).toString());
                i = i + 1;
            }

            Intent mainIntent = new Intent(QuestionTestActivity.this, LevelCompleted.class);
            mainIntent.putExtra("LIST_ANSWERS", listArray);
            mainIntent.putExtra("isFromTutorial", isfromtutorail);
            mainIntent.putExtra("arrayResponses", responses.toString());
            String header = new String();
            if (isfromtutorail) {
                header = "Tutorial";

            } else {
                header = subcategory.getName() + " - Level " + levelSelected.getName();
            }
            mainIntent.putExtra("LevelCompletedHeader", header);

            if (!isfromtutorail) {
                mainIntent.putExtra("lvlcompleted", levelSelected.getName());
                mainIntent.putExtra("subcategory", subcategory);
                mainIntent.putExtra("Level", levelSelected);
            }
            QuestionTestActivity.this.startActivity(mainIntent);
            QuestionTestActivity.this.finish();
        }

    }
}

