package com.devteach.devshaker;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.devteach.devshaker.model.Badge;
import com.devteach.devshaker.model.DevShackerDbHelper;
import com.devteach.devshaker.model.User;
import com.devteach.devshaker.utils.MenuActivityase;
import com.devteach.devshaker.utils.Utils;

import java.util.LinkedList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by mpereira on 5/6/15.
 */
public class BadgesActivity extends MenuActivityase {

    private static  DevShackerDbHelper dbHelper;
    private Context context;
    private Activity act;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
        this.act = this;

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_badges, contentFrameLayout);

        TextView userNameLabel = (TextView)findViewById(R.id.badge_user_name);
        dbHelper = DevShackerDbHelper.getInstance(this);

        User user = User.getUserFromDDBB(dbHelper.getReadableDatabase());

        List<Badge> listaBadgesOfUser = Badge.getBadgesFromDDBB(dbHelper.getReadableDatabase());
        List <LinearLayout> layoutListForCode = new LinkedList<LinearLayout>();
        List <LinearLayout> layoutListForBuild = new LinkedList<LinearLayout>();
        List <LinearLayout> layoutListForThink = new LinkedList<LinearLayout>();

        for(Badge badge: listaBadgesOfUser){
            String type = badge.getCategoryName().toLowerCase();

            switch (type){
                case "code":
                    layoutListForCode.add(createLinearLayouForBadge(badge));
                    break;
                case "build":
                    layoutListForBuild.add(createLinearLayouForBadge(badge));
                    break;
                case "think":
                    layoutListForThink.add(createLinearLayouForBadge(badge));
                    break;
                default:
                    break;
            }
        }

        if(layoutListForCode.size()>0) findViewById(R.id.text_no_code_badge).setVisibility(View.GONE);
        if(layoutListForBuild.size()>0) findViewById(R.id.text_no_code_build).setVisibility(View.GONE);
        if(layoutListForThink.size()>0) findViewById(R.id.text_no_code_think).setVisibility(View.GONE);


        userNameLabel.setText(user.getScreenName());
        MenuActivityase.setToolbarColor("default");
        MenuActivityase.setActivityTitlebyResource(R.string.menu_badges);

        CircleImageView circleImageView = (CircleImageView) findViewById(R.id.profile_circle_pic);
        Utils.loadImageFromStorage(circleImageView, user.getImg());

        LinearLayout frame= (LinearLayout)findViewById(R.id.frame_code);
        for(LinearLayout ll : layoutListForCode ){
            frame.addView(ll);
        }

        frame= (LinearLayout)findViewById(R.id.frame_build);
        for(LinearLayout ll : layoutListForBuild){
            frame.addView(ll);
        }
        frame= (LinearLayout)findViewById(R.id.frame_think);
        for(LinearLayout ll : layoutListForThink){
            frame.addView(ll);
        }

    }


    private LinearLayout createLinearLayouForBadge(final Badge badge){
        LinearLayout badgeContainer = new LinearLayout(context);


        badgeContainer.setLayoutParams(
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT));
        badgeContainer.setOrientation(LinearLayout.VERTICAL);

        ImageView badgeImage = new ImageView(context);

        LinearLayout.LayoutParams imageLP = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,0.2f);
        imageLP.gravity= Gravity.CENTER;
        //imageLP.weight=3;
        imageLP.setMargins(5,5,5,5);
        badgeImage.setLayoutParams(imageLP);

        Bitmap myBitmap = BitmapFactory.decodeFile(badge.getImg());
        badgeImage.setImageBitmap(myBitmap);
        TextView subcategoryText = new TextView(context);

        LinearLayout.LayoutParams textLP = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,0.9f);
        //textLP.setMargins(0,0,0,15);

        textLP.gravity= Gravity.CENTER_HORIZONTAL;
        //textLP.weight=1;
        subcategoryText.setGravity(Gravity.CENTER);
        subcategoryText.setLayoutParams(textLP);

        subcategoryText.setText(badge.getSubcategoryName());
        subcategoryText.setTextColor(context.getResources().getColor(R.color.primary));


        badgeContainer.addView(badgeImage);
        badgeContainer.addView(subcategoryText);

        badgeContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Badge> badgeList = new LinkedList<>();
                badgeList.add(badge);
               Utils.showSortPopup(act,badgeList);
            }
        });

        return badgeContainer;
    }

}
