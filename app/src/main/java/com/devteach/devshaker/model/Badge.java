package com.devteach.devshaker.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mpereira on 14/3/15.
 */
public class Badge implements Serializable{

    private Long ID;
    private String name;
    private String img;
    private String description;
    private long dateGiven;
    private String subcategoryName;
    private String categoryName;

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public long getDateGiven() {
        return dateGiven;
    }

    public void setDateGiven(long dateGiven) {
        this.dateGiven = dateGiven;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubcategoryName() {
        return subcategoryName;
    }

    public void setSubcategoryName(String subcategoryName) {
        this.subcategoryName = subcategoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Badge(Long ID, String name, String description, String img, long date, String subcategoryName, String categoryName) {
        this.ID = ID;
        this.name = name;
        this.description = description;
        this.img = img;
        this.dateGiven = date;
        this.subcategoryName = subcategoryName;
        this.categoryName = categoryName;
    }


    public static List<Badge> getBadgesFromDDBB(SQLiteDatabase db){

        String selectQuery = "SELECT * FROM " + Badge.BadgeEntry.TABLE_NAME+";";
        System.out.println(selectQuery);
        Cursor cursor = db.rawQuery(selectQuery, null);
        Badge badge = null;
        List<Badge> badgesList = new LinkedList<>();
        if (cursor.moveToFirst()) {
            do {
                badge = new Badge(Long.parseLong(cursor.getString(0)),cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getLong(4),cursor.getString(5),cursor.getString(6));
                badgesList.add(badge);
            } while (cursor.moveToNext());

        }
        cursor.close();
        return badgesList;
    }

    public static long saveBadgeToDDBB(Badge badge, SQLiteDatabase db){
        ContentValues initialValues = new ContentValues();
        long returnValue=0;


        initialValues.put(BadgeEntry.COLUMN_NAME_BADGE_ID, badge.getID());
        initialValues.put(BadgeEntry.COLUMN_NAME_BADGE_NAME, badge.getName());
        initialValues.put(BadgeEntry.COLUMN_NAME_BADGE_DESC, badge.getDescription());
        initialValues.put(BadgeEntry.COLUMN_NAME_BADGE_IMG, badge.getImg());
        initialValues.put(BadgeEntry.COLUMN_NAME_BADGE_DATE, badge.getDateGiven());
        initialValues.put(BadgeEntry.COLUMN_NAME_BADGE_SUBCATEGORY, badge.getSubcategoryName());
        initialValues.put(BadgeEntry.COLUMN_NAME_BADGE_CATEGORY, badge.getCategoryName());



        returnValue = db.insert(Badge.BadgeEntry.TABLE_NAME,
                BadgeEntry.COLUMN_NAME_BADGE_ID, initialValues);

        System.out.println("ReturnValue: " + returnValue);
        Log.d("BADGE", "BADGE con nombre " + badge.getName() + " anadido correctamente");
        return returnValue;
    }

    public static abstract class BadgeEntry implements BaseColumns {
        public static final String TABLE_NAME = "badge";
        public static final String COLUMN_NAME_BADGE_ID = "Id";
        public static final String COLUMN_NAME_BADGE_NAME = "name";
        public static final String COLUMN_NAME_BADGE_DESC = "description";
        public static final String COLUMN_NAME_BADGE_IMG = "img";
        public static final String COLUMN_NAME_BADGE_DATE = "date";
        public static final String COLUMN_NAME_BADGE_SUBCATEGORY = "subcategory";
        public static final String COLUMN_NAME_BADGE_CATEGORY = "category";

    }
}
