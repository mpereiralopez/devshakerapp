package com.devteach.devshaker.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by mpereira on 4/6/15.
 */
public class Category {

    private long id;
    private String name;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category(long id, String name) {
        this.id = id;
        this.name = name;
    }


    public static List<Category> getCategoriesFromDDBB(SQLiteDatabase db){

        String selectQuery = "SELECT * FROM " + CategoryEntry.TABLE_NAME+";";
        System.out.println(selectQuery);
        Cursor cursor = db.rawQuery(selectQuery, null);
        Category category = null;
        List<Category> categoryList = new LinkedList<Category>();
        if (cursor.moveToFirst()) {
            do {
                category = new Category(Long.parseLong(cursor.getString(0)),cursor.getString(1));
                categoryList.add(category);
            } while (cursor.moveToNext());

        }
        cursor.close();
        return categoryList;
    }

    public static Category getCategoryFromDDBB(SQLiteDatabase db, long catId){

        String selectQuery = "SELECT * FROM " + CategoryEntry.TABLE_NAME+" WHERE "+ CategoryEntry.COLUMN_NAME_CATEGORY_ID+"="+catId+";";
        System.out.println(selectQuery);
        Cursor cursor = db.rawQuery(selectQuery, null);
        Category category = null;
        if (cursor.moveToFirst()) {

            category = new Category(Long.parseLong(cursor.getString(0)),cursor.getString(1));
        }
        cursor.close();
        return category;
    }

    public static long saveCategoryToDDBB(Category c, SQLiteDatabase db){
        ContentValues initialValues = new ContentValues();
        long returnValue=0;


        initialValues.put(CategoryEntry.COLUMN_NAME_CATEGORY_ID, c.getId());
        initialValues.put(CategoryEntry.COLUMN_NAME_CATEGORY_NAME, c.getName());


        returnValue = db.insert(CategoryEntry.TABLE_NAME,
                CategoryEntry.COLUMN_NAME_CATEGORY_ID, initialValues);

        System.out.println("ReturnValue: " + returnValue);
        Log.d("CATEGORY", "CATEGORIA con nombre " + c.getName() + " anadido correctamente");
        return returnValue;
    }

    public static abstract class CategoryEntry implements BaseColumns {
        public static final String TABLE_NAME = "category";
        public static final String COLUMN_NAME_CATEGORY_ID = "ID";
        public static final String COLUMN_NAME_CATEGORY_NAME = "name";
    }
}
