package com.devteach.devshaker.model;

import android.provider.BaseColumns;

import java.io.Serializable;

/**
 * Created by ted on 13/08/2016.
 */
public class UserQuestionResult implements Serializable {

    private static final long serialVersionUID = 1L;

    private long userId;
    private long questionId;
    private String arrayResponsesUser;
    private int xpGained;


    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public String getArrayResponsesUser() {
        return arrayResponsesUser;
    }

    public void setArrayResponsesUser(String arrayResponsesUser) {
        this.arrayResponsesUser = arrayResponsesUser;
    }

    public int getXpGained() {
        return xpGained;
    }

    public void setXpGained(int xpGained) {
        this.xpGained = xpGained;
    }

    public UserQuestionResult(long userId, long questionId, String arrayResponsesUser, int xpGained) {
        this.userId = userId;
        this.questionId = questionId;
        this.arrayResponsesUser = arrayResponsesUser;
        this.xpGained = xpGained;
    }

    public static abstract class UserQuestionResultEntry implements BaseColumns {
        public static final String TABLE_NAME = "UserQuestionResult";
        public static final String COLUMN_NAME_USER_ID = "userId";
        public static final String COLUMN_NAME_QUESTION_ID = "questionId";
        public static final String COLUMN_NAME_ARRAY_RESPONSES_USER = "arrayResponsesUser";
        public static final String COLUMN_NAME_XP_GAINED = "xp";

    }
}
