package com.devteach.devshaker.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by mpereira on 6/8/15.
 */
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *  {"agreedToTermsOfUse":true,"comments":"","companyId":"20116","contactId":"30157","createDate":1466620506828,
     *  "defaultUser":false,"emailAddress":"test@devshaker.com","emailAddressVerified":true,
     *  "facebookId":"0","failedLoginAttempts":0,"firstName":"Test","googleUserId":"","graceLoginCount":0,"greeting":"Welcome Test Test!",
     *  "jobTitle":"","languageId":"en_GB","lastFailedLoginDate":1469818372917,"lastLoginDate":1467750954364,"lastLoginIP":"127.0.0.1","lastName":"Test",
     *  "ldapServerId":"-1","lockout":false,"lockoutDate":null,"loginDate":1467881242407,"loginIP":"127.0.0.1","middleName":"","modifiedDate":1469818662915,
     *  "mvccVersion":"54","openId":"","portraitId":"0","reminderQueryAnswer":"Pazos",
     *  "reminderQueryQuestion":"what-is-your-father's-middle-name","screenName":"test1","status":0,
     *  "timeZoneId":"UTC","userId":"30156","uuid":"89545555-bfb5-a189-79a8-5de9d4d89b37"}

     */

    private long userId;
    private long companyId;
    private String firstName;
    private String emailAddress;
    private String languageId;
    private String lastName;
    private String screenName;
    private String password;
    private int isMale;
    private long birthdate;
    private String country;
    private String middleName;
    private String uuid;
    private long portraitId;
    private String imgUrl;
    private int xp;
    private String phone;

    public User(long userId, long companyId, String firstName, String emailAddress, String lastName, String screenName,String middleName,
               String languageId, String password, int isMale, long birthdate, String country, String uuid,long portraitId ,String imgUrl ,String phone,int xp) {
        this.userId = userId;
        this.companyId = companyId;
        this.firstName = firstName;
        this.emailAddress = emailAddress;
        this.lastName = lastName;
        this.screenName = screenName;
        this.middleName = middleName;
        this.languageId = languageId;
        this.password = password;
        this.isMale = isMale;
        this.birthdate = birthdate;
        this.country = country;
        this.uuid = uuid;
        this.portraitId=portraitId;
        this.imgUrl=imgUrl;
        this.phone = phone;
        this.xp = xp;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getLanguageId() {
        return languageId;
    }

    public void setLanguageId(String languageId) {
        this.languageId = languageId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int isMale() {
        return isMale;
    }

    public void setMale(int male) {
        isMale = male;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public long getPortraitId() {
        return portraitId;
    }

    public void setPortraitId(long portraitId) {
        this.portraitId = portraitId;
    }

    public long getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(long birthdate) {
        this.birthdate = birthdate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getImg() {
        return imgUrl;
    }

    public void setImg(String img) {
        this.imgUrl = img;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public static long saveUserToDDBB(User u, SQLiteDatabase db){
        ContentValues initialValues = new ContentValues();
        long returnValue=0;

        initialValues.put(UserEntry.COLUMN_NAME_USER_ID, u.getUserId());
        initialValues.put(UserEntry.COLUMN_NAME_COMPANY_ID, u.getCompanyId());
        initialValues.put(UserEntry.COLUMN_NAME_FIRST_NAME, u.getFirstName());
        initialValues.put(UserEntry.COLUMN_NAME_EMAIL, u.getEmailAddress());
        initialValues.put(UserEntry.COLUMN_NAME_LAST_NAME, u.getLastName());
        initialValues.put(UserEntry.COLUMN_NAME_SCREEN_NAME, u.getScreenName());
        initialValues.put(UserEntry.COLUMN_NAME_MIDDLE_NAME, u.getMiddleName());
        initialValues.put(UserEntry.COLUMN_NAME_LANGUAGEID, u.getLanguageId());
        initialValues.put(UserEntry.COLUMN_NAME_PASSWORD, u.getPassword());
        initialValues.put(UserEntry.COLUMN_NAME_GENDER, u.isMale());
        initialValues.put(UserEntry.COLUMN_NAME_BIRTHDATE, u.getBirthdate());
        initialValues.put(UserEntry.COLUMN_NAME_COUNTRY, u.getCountry());
        initialValues.put(UserEntry.COLUMN_NAME_UUID, u.getUuid());
        initialValues.put(UserEntry.COLUMN_NAME_PORTRAIT_ID, u.getPortraitId());
        initialValues.put(UserEntry.COLUMN_NAME_PHONE, u.getPhone());
        initialValues.put(UserEntry.COLUMN_NAME_COUNTRY, u.getCountry());
        initialValues.put(UserEntry.COLUMN_NAME_IMG, u.getImg());


        db.replace(UserEntry.TABLE_NAME,UserEntry.COLUMN_NAME_USER_ID,initialValues);
        returnValue = db.insertWithOnConflict(UserEntry.TABLE_NAME,
                UserEntry.COLUMN_NAME_USER_ID, initialValues,SQLiteDatabase.CONFLICT_REPLACE);



        System.out.println("ReturnValue: " + returnValue);
        //db.close();
        Log.d("USER", "Usuario con nombre " + u.getFirstName() + " anadido correctamente");
        return returnValue;
    }


    public static User getUserFromDDBB(SQLiteDatabase db){

        String selectQuery = "SELECT * FROM " + UserEntry.TABLE_NAME+" LIMIT 1;";
        System.out.println(selectQuery);
        Cursor cursor = db.rawQuery(selectQuery, null);
        User u = null;
        if (cursor.moveToFirst()) {
            int uxp = 0;

            if(cursor.getString(16)!=null){
                uxp = Integer.parseInt(cursor.getString(16));
            }
            u = new User(Long.parseLong(cursor.getString(0)),
                    Long.parseLong(cursor.getString(1)),cursor.getString(2),
                    cursor.getString(3),cursor.getString(4),
                    cursor.getString(5),cursor.getString(6),
                    cursor.getString(7),cursor.getString(8),
                    Integer.parseInt(cursor.getString(9)),Long.parseLong(cursor.getString(10)),
                    cursor.getString(11),cursor.getString(12),Long.parseLong(cursor.getString(13)),
                    cursor.getString(14),cursor.getString(15),uxp);
        }

        cursor.close();
        return u;
    }

    public static void updateUserProfilePic(long userId, String newImg, SQLiteDatabase db){
        Log.d("USER", "updateUserXp");
        //Cursor cursor = db.rawQuery(updateQuery, null);
        ContentValues valores = new ContentValues();
        valores.put(UserEntry.COLUMN_NAME_IMG, newImg);
        String whereClausule = UserEntry.COLUMN_NAME_USER_ID+"="+userId;
        int returnValue = db.update(UserEntry.TABLE_NAME, valores, whereClausule ,null);
        Log.d("USER",returnValue+"");
    }


    public static void updateUserXp(long userId ,int newXp,SQLiteDatabase db){


        Log.d("USER", "updateUserXp");
        //Cursor cursor = db.rawQuery(updateQuery, null);
        ContentValues valores = new ContentValues();
        valores.put(UserEntry.COLUMN_NAME_XP, newXp);
        String whereClausule = UserEntry.COLUMN_NAME_USER_ID+"="+userId;
        int returnValue = db.update(UserEntry.TABLE_NAME, valores, whereClausule ,null);
        Log.d("USER",returnValue+"");
    }


    public static void updateUserFields(long userId , HashMap<String, Object> map, SQLiteDatabase db){
        Log.d("USER", "updateUserFields");
        ContentValues valores = new ContentValues();
        Iterator it = map.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
            if (pair.getValue() instanceof String){
                valores.put (pair.getKey().toString(), pair.getValue().toString());
            }else{
                if (pair.getValue() instanceof Integer){
                    valores.put (pair.getKey().toString(), (int)pair.getValue());
                }else{
                    if (pair.getValue() instanceof Long){
                        valores.put (pair.getKey().toString(), (long)pair.getValue());
                    }
                }
            }

        }

        String whereClausule = UserEntry.COLUMN_NAME_USER_ID+"="+userId;
        int returnValue = db.update(UserEntry.TABLE_NAME, valores, whereClausule ,null);
        Log.d("USER",returnValue+"");
    }

    public static abstract class UserEntry implements BaseColumns {
        public static final String TABLE_NAME = "Users";
        public static final String COLUMN_NAME_USER_ID = "userId";
        public static final String COLUMN_NAME_COMPANY_ID = "companyId";
        public static final String COLUMN_NAME_FIRST_NAME = "firstName";
        public static final String COLUMN_NAME_EMAIL = "emailAddress";
        public static final String COLUMN_NAME_LAST_NAME = "lastName";
        public static final String COLUMN_NAME_SCREEN_NAME = "screenName";
        public static final String COLUMN_NAME_MIDDLE_NAME = "middleName";
        public static final String COLUMN_NAME_LANGUAGEID ="languageId";
        public static final String COLUMN_NAME_PASSWORD = "password";
        public static final String COLUMN_NAME_GENDER = "isMale";
        public static final String COLUMN_NAME_BIRTHDATE = "birthdate";
        public static final String COLUMN_NAME_COUNTRY = "country";
        public static final String COLUMN_NAME_PHONE = "phone";
        public static final String COLUMN_NAME_IMG = "img";
        public static final String COLUMN_NAME_PORTRAIT_ID = "portraitId";
        public static final String COLUMN_NAME_UUID = "uuid";
        public static final String COLUMN_NAME_XP = "xp";

    }
}
