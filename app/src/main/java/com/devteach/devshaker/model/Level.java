package com.devteach.devshaker.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by je10413 on 11/06/2015.
 */
public class Level implements Serializable {

    private static final long serialVersionUID = 1L;


    public static final String LEVEL_STATUS_BLOQUED = "bloqued";
    public static final String LEVEL_STATUS_NOT_STARTED = "not_started";
    public static final String LEVEL_STATUS_STARTED = "started";
    public static final String LEVEL_STATUS_COMPLETED = "completed";

    private static final String TAG="LEVEL_POJO";

    private long ID;
    private String name;
    private long subCategory;
    private String status;
    private int numberOfQuestions;
    private int percentage;

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(long subCategory) {
        this.subCategory = subCategory;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getNumberOfQuestions() {
        return numberOfQuestions;
    }

    public void setNumberOfQuestions(int numberOfQuestions) {
        this.numberOfQuestions = numberOfQuestions;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public Level(long ID, String name, long subCategory, String status, int numberOfQuestions, int percentage) {
        this.ID = ID;
        this.name = name;
        this.subCategory = subCategory;
        this.status = status;
        this.numberOfQuestions = numberOfQuestions;
        this.percentage = percentage;
    }

    public static List<Level> getLevelsOfSubcategory(long subcategoryId, SQLiteDatabase db){

        String selectQuery = "SELECT * FROM " + LevelEntry.TABLE_NAME+" WHERE "+
                LevelEntry.COLUMN_NAME_SUBCATEGORY_ID+"="+subcategoryId+";";
        System.out.println(selectQuery);
        Cursor cursor = db.rawQuery(selectQuery, null);
        Level level = null;
        List<Level> list = new LinkedList<Level>();
        if (cursor.moveToFirst()) {
            do {
                //public Question(long ID, long ID_SubCat, String textQuestion, String imageQuestion, String answer1, String answer2, String answer3, String answer4, String rightAnswers, int difficulty)
                level = new Level(
                        Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1),
                        Long.parseLong(cursor.getString(2)),
                        cursor.getString(3),
                        Integer.parseInt(cursor.getString(4)),
                        Integer.parseInt(cursor.getString(5))
                );

                list.add(level);
            } while (cursor.moveToNext());
            return list;

        }else{
            return null;
        }

    }

    public static Level getLevelByLevelSubCatAndName(int subCategory, int levelName, SQLiteDatabase db){
        String selectQuery = "SELECT * FROM " + LevelEntry.TABLE_NAME+" WHERE "+
                LevelEntry.COLUMN_NAME_NAME+"="+levelName+" AND "+LevelEntry.COLUMN_NAME_SUBCATEGORY_ID+" = "+subCategory+";";
        System.out.println(selectQuery);
        Cursor cursor = db.rawQuery(selectQuery, null);

        Level level = null;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            level = new Level(
                    Integer.parseInt(cursor.getString(0)),
                    cursor.getString(1),
                    Long.parseLong(cursor.getString(2)),
                    cursor.getString(3),
                    Integer.parseInt(cursor.getString(4)),
                    Integer.parseInt(cursor.getString(5))
            );
            return level;

        }else{
            return null;
        }
    }

    public static int getTotalLevelsOfSubcategory (long subcategoryId, SQLiteDatabase db){
        int returnValue = 0;

        String selectQuery = "SELECT * FROM " + LevelEntry.TABLE_NAME+" WHERE "+
                LevelEntry.COLUMN_NAME_SUBCATEGORY_ID+"="+subcategoryId+";";
        System.out.println(selectQuery);
        Cursor cursor = db.rawQuery(selectQuery, null);
        returnValue = cursor.getCount();
        cursor.close();
        return returnValue;
    }


    public static long saveLevelsToDDBB(Level level, SQLiteDatabase db){
        ContentValues initialValues = new ContentValues();
        long returnValue=0;
        initialValues.put(LevelEntry.COLUMN_NAME_LEVEL, level.getID());
        initialValues.put(LevelEntry.COLUMN_NAME_NAME, level.getName());
        initialValues.put(LevelEntry.COLUMN_NAME_SUBCATEGORY_ID, level.getSubCategory());
        initialValues.put(LevelEntry.COLUMN_NAME_NUMBER_OF_QUESTIONS, level.getNumberOfQuestions());
        initialValues.put(LevelEntry.COLUMN_NAME_STATUS, level.getStatus());
        initialValues.put(LevelEntry.COLUMN_NAME_PERCENTAGE_PASSED,0);

        returnValue = db.insertWithOnConflict(LevelEntry.TABLE_NAME,
                LevelEntry.COLUMN_NAME_LEVEL, initialValues, SQLiteDatabase.CONFLICT_REPLACE);
        System.out.println("ReturnValue: " + returnValue);

        Log.d("Level", "Level " + level.getID() + " anadida correctamente");
        return returnValue;
    }

    public static void updateLevelStatusAndPercentage(Level level,int percentage,SQLiteDatabase db){



        Log.d(TAG, "updateLevelStatus");
        //Cursor cursor = db.rawQuery(updateQuery, null);
        ContentValues valores = new ContentValues();
        valores.put(LevelEntry.COLUMN_NAME_STATUS, LEVEL_STATUS_COMPLETED);
        valores.put(LevelEntry.COLUMN_NAME_PERCENTAGE_PASSED, percentage);
        String whereClausule = LevelEntry.COLUMN_NAME_LEVEL+"="+level.getID();
        int returnValue = db.update(LevelEntry.TABLE_NAME, valores, whereClausule ,null);

        //Ahora necesito desbloquear el siguiente nivel

        List<Level> listLevel =  Level.getLevelsOfSubcategory(level.getSubCategory(),db);
        int i = 0;
        for(Level l: listLevel){
            if(l.getID() == level.getID()){
                int aux = i+1;
                if(aux<listLevel.size()){
                    Level modified = listLevel.get(aux);
                    modified.setStatus(LEVEL_STATUS_NOT_STARTED);
                    ContentValues valores2 = new ContentValues();
                    valores2.put(LevelEntry.COLUMN_NAME_STATUS, modified.getStatus());
                    String whereClausule2 = LevelEntry.COLUMN_NAME_LEVEL+"="+modified.getID();
                    db.update(LevelEntry.TABLE_NAME, valores2, whereClausule2 ,null);
                }
            }
            i++;
        }
    }


    public static String getLevelProgressInfoForSubcategoryActivity(long subCategoryId, SQLiteDatabase db){
        String returnValue = new String();
        List<Level> levelList = getLevelsOfSubcategory(subCategoryId, db);
        int totalLevels = levelList.size();
        int value = 0;
        for(int i= 0;i<totalLevels;i++){
            if(levelList.get(i).getStatus().equalsIgnoreCase(LEVEL_STATUS_COMPLETED))value++;
        }
        returnValue = "Level "+value+"/"+totalLevels;
        return returnValue;
    }


    public static abstract class LevelEntry implements BaseColumns {
        public static final String TABLE_NAME = "Level";
        public static final String COLUMN_NAME_LEVEL = "ID";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_SUBCATEGORY_ID= "subCategory";
        public static final String COLUMN_NAME_STATUS= "status";
        public static final String COLUMN_NAME_NUMBER_OF_QUESTIONS = "numberOfQuestions";
        public static final String COLUMN_NAME_PERCENTAGE_PASSED = "percentage";


    }
}
