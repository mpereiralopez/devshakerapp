package com.devteach.devshaker.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by mpereira on 14/3/15.
 */
public class DevShackerDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "DevShaker.db";

    private static final String TEXT_TYPE= " TEXT";
    private static final String INTEGER_TYPE= " INTEGER";
    private static final String BLOB_TYPE= " BLOB";
    private static final String COMMA_SEP = ",";
    private static DevShackerDbHelper mInstance = null;


    /** HERE I PUT CREATE SENTECENS **/

    private static final String SQL_CREATE_CATEGORY =
            "CREATE TABLE IF NOT EXISTS " + Category.CategoryEntry.TABLE_NAME + " (" +
                    Category.CategoryEntry.COLUMN_NAME_CATEGORY_ID + " INTEGER PRIMARY KEY," +
                    Category.CategoryEntry.COLUMN_NAME_CATEGORY_NAME + TEXT_TYPE +");";

    //Challenge (dates are put like integer --> Number of millisendos)
    private static final String SQL_CREATE_SUBCATEGORY =
            "CREATE TABLE IF NOT EXISTS " + Subcategory.SubcategoryEntry.TABLE_NAME + " (" +
                    Subcategory.SubcategoryEntry.COLUMN_NAME_SUBCATEGORY_ID + " INTEGER PRIMARY KEY," +
                    Subcategory.SubcategoryEntry.COLUMN_NAME_CATEGORY_ID + INTEGER_TYPE + COMMA_SEP +
                    Subcategory.SubcategoryEntry.COLUMN_NAME_SUBCATEGORY_NAME + TEXT_TYPE + COMMA_SEP +
                    "FOREIGN KEY("+ Subcategory.SubcategoryEntry.COLUMN_NAME_CATEGORY_ID+") REFERENCES "+Category.CategoryEntry.TABLE_NAME+" ("+Category.CategoryEntry.COLUMN_NAME_CATEGORY_ID +"));";


    private static final String SQL_CREATE_LEVELS =
            "CREATE TABLE IF NOT EXISTS "+ Level.LevelEntry.TABLE_NAME+" ("+
                    Level.LevelEntry.COLUMN_NAME_LEVEL+INTEGER_TYPE+COMMA_SEP+
                    Level.LevelEntry.COLUMN_NAME_NAME+TEXT_TYPE+COMMA_SEP+
                    Level.LevelEntry.COLUMN_NAME_SUBCATEGORY_ID+INTEGER_TYPE+COMMA_SEP+
                    Level.LevelEntry.COLUMN_NAME_STATUS+TEXT_TYPE+COMMA_SEP+
                    Level.LevelEntry.COLUMN_NAME_NUMBER_OF_QUESTIONS+INTEGER_TYPE+COMMA_SEP+
                    Level.LevelEntry.COLUMN_NAME_PERCENTAGE_PASSED+INTEGER_TYPE+COMMA_SEP+
                    "FOREIGN KEY("+ Level.LevelEntry.COLUMN_NAME_SUBCATEGORY_ID+") REFERENCES "+Subcategory.SubcategoryEntry.TABLE_NAME+" ("+Subcategory.SubcategoryEntry.COLUMN_NAME_SUBCATEGORY_ID +")" +COMMA_SEP+
                    "PRIMARY KEY("+ Level.LevelEntry.COLUMN_NAME_LEVEL+","+Level.LevelEntry.COLUMN_NAME_SUBCATEGORY_ID+")"
                    +");";


    //Challenge (dates are put like integer --> Number of millisendos)
    private static final String SQL_CREATE_CHALLENGE =
            "CREATE TABLE IF NOT EXISTS " + Challenge.ChallengeEntry.TABLE_NAME + " (" +
                    Challenge.ChallengeEntry.COLUMN_NAME_CHALLENGE_ID + " INTEGER PRIMARY KEY," +
                    Challenge.ChallengeEntry.COLUMN_NAME_CHALLENGE_COMPANY_ID + INTEGER_TYPE + COMMA_SEP +
                    Challenge.ChallengeEntry.COLUMN_NAME_CHALLENGE_NAME + TEXT_TYPE + COMMA_SEP +
                    Challenge.ChallengeEntry.COLUMN_NAME_CHALLENGE_LEVEL + INTEGER_TYPE + COMMA_SEP +
                    Challenge.ChallengeEntry.COLUMN_NAME_CHALLENGE_NQUESTIONS + INTEGER_TYPE + COMMA_SEP +
                    Challenge.ChallengeEntry.COLUMN_NAME_CHALLENGE_DESC + TEXT_TYPE + COMMA_SEP +
                    Challenge.ChallengeEntry.COLUMN_NAME_CHALLENGE_STARTDATE + INTEGER_TYPE + COMMA_SEP +
                    Challenge.ChallengeEntry.COLUMN_NAME_CHALLENGE_ENDDATE+ INTEGER_TYPE + COMMA_SEP +
                    "FOREIGN KEY("+Challenge.ChallengeEntry.COLUMN_NAME_CHALLENGE_COMPANY_ID+") REFERENCES "+ Company.CompanyEntry.TABLE_NAME+"("+ Company.CompanyEntry.COLUMN_NAME_COMPANY_ID+"));";

    //Questions

    private static final String SQL_CREATE_QUESTION =
            "CREATE TABLE IF NOT EXISTS "+ Question.QuestionEntry.TABLE_NAME+" ("+
                    Question.QuestionEntry.COLUMN_NAME_QUESTION_ID+" INTEGER PRIMARY KEY"+COMMA_SEP+
                    Question.QuestionEntry.COLUMN_NAME_SUBCATEGORY_ID+TEXT_TYPE+COMMA_SEP+
                    Question.QuestionEntry.COLUMN_NAME_QUESTION_TEXT+TEXT_TYPE+COMMA_SEP+
                    Question.QuestionEntry.COLUMN_NAME_QUESTION_IMAGE_URL+TEXT_TYPE+COMMA_SEP+
                    Question.QuestionEntry.COLUMN_NAME_ANSWER1+TEXT_TYPE+COMMA_SEP+
                    Question.QuestionEntry.COLUMN_NAME_ANSWER2+TEXT_TYPE+COMMA_SEP+
                    Question.QuestionEntry.COLUMN_NAME_ANSWER3+TEXT_TYPE+COMMA_SEP+
                    Question.QuestionEntry.COLUMN_NAME_ANSWER4+TEXT_TYPE+COMMA_SEP+
                    Question.QuestionEntry.COLUMN_NAME_RIGHT_ANSWERS+TEXT_TYPE+COMMA_SEP+
                    Question.QuestionEntry.COLUMN_NAME_DIFFICULTY+TEXT_TYPE+COMMA_SEP+
                    "FOREIGN KEY("+ Question.QuestionEntry.COLUMN_NAME_SUBCATEGORY_ID+") REFERENCES "+Subcategory.SubcategoryEntry.TABLE_NAME+" ("+Subcategory.SubcategoryEntry.COLUMN_NAME_SUBCATEGORY_ID +"));";


    //Questions of Challenge

    private static final String SQL_CREATE_QUESTIONS_OF_CHALLENGE =
            "CREATE TABLE IF NOT EXISTS "+ Challenge.QuestionOfChallenge.TABLE_NAME+" ("+
                    Challenge.QuestionOfChallenge.COLUMN_NAME_CHALLENGE_ID+" INTEGER PRIMARY KEY"+COMMA_SEP+
                    Challenge.QuestionOfChallenge.COLUMN_NAME_CHALLENGE_COMPANY_ID+INTEGER_TYPE+" );";


    private static final String SQL_CREATE_USER =
            "CREATE TABLE IF NOT EXISTS "+ User.UserEntry.TABLE_NAME+" ("+
                    User.UserEntry.COLUMN_NAME_USER_ID+" INTEGER PRIMARY KEY"+COMMA_SEP+
                    User.UserEntry.COLUMN_NAME_COMPANY_ID+INTEGER_TYPE+COMMA_SEP+
                    User.UserEntry.COLUMN_NAME_FIRST_NAME+TEXT_TYPE+COMMA_SEP+
                    User.UserEntry.COLUMN_NAME_EMAIL+TEXT_TYPE+COMMA_SEP+
                    User.UserEntry.COLUMN_NAME_LAST_NAME+TEXT_TYPE+COMMA_SEP+
                    User.UserEntry.COLUMN_NAME_SCREEN_NAME+TEXT_TYPE+COMMA_SEP+
                    User.UserEntry.COLUMN_NAME_MIDDLE_NAME+TEXT_TYPE+COMMA_SEP+
                    User.UserEntry.COLUMN_NAME_LANGUAGEID+TEXT_TYPE+COMMA_SEP+
                    User.UserEntry.COLUMN_NAME_PASSWORD+TEXT_TYPE+COMMA_SEP+
                    User.UserEntry.COLUMN_NAME_GENDER+INTEGER_TYPE+COMMA_SEP+
                    User.UserEntry.COLUMN_NAME_BIRTHDATE+TEXT_TYPE+COMMA_SEP+
                    User.UserEntry.COLUMN_NAME_COUNTRY+TEXT_TYPE+COMMA_SEP+
                    User.UserEntry.COLUMN_NAME_UUID+TEXT_TYPE+COMMA_SEP+
                    User.UserEntry.COLUMN_NAME_PORTRAIT_ID+INTEGER_TYPE+COMMA_SEP+
                    User.UserEntry.COLUMN_NAME_IMG+TEXT_TYPE+COMMA_SEP+
                    User.UserEntry.COLUMN_NAME_PHONE+TEXT_TYPE+COMMA_SEP+
                    User.UserEntry.COLUMN_NAME_XP+INTEGER_TYPE+" );";

    //Challenge (dates are put like integer --> Number of millisendos)
    private static final String SQL_CREATE_USERQUESTIONSRESULT =
            "CREATE TABLE IF NOT EXISTS " + UserQuestionResult.UserQuestionResultEntry.TABLE_NAME + " (" +
                    UserQuestionResult.UserQuestionResultEntry.COLUMN_NAME_USER_ID + INTEGER_TYPE + COMMA_SEP +
                    UserQuestionResult.UserQuestionResultEntry.COLUMN_NAME_QUESTION_ID + INTEGER_TYPE + COMMA_SEP +
                    UserQuestionResult.UserQuestionResultEntry.COLUMN_NAME_ARRAY_RESPONSES_USER + TEXT_TYPE + COMMA_SEP +
                    UserQuestionResult.UserQuestionResultEntry.COLUMN_NAME_XP_GAINED + INTEGER_TYPE + COMMA_SEP +
                    "FOREIGN KEY("+ UserQuestionResult.UserQuestionResultEntry.COLUMN_NAME_USER_ID+") REFERENCES "+User.UserEntry.TABLE_NAME+" ("+User.UserEntry.COLUMN_NAME_USER_ID +")," +
                    "FOREIGN KEY("+ Question.QuestionEntry.COLUMN_NAME_QUESTION_ID+") REFERENCES "+Question.QuestionEntry.TABLE_NAME+" ("+Question.QuestionEntry.COLUMN_NAME_QUESTION_ID +"));";


    private static final String SQL_CREATE_BADGE =
            "CREATE TABLE IF NOT EXISTS " + Badge.BadgeEntry.TABLE_NAME + " (" +
                    Badge.BadgeEntry.COLUMN_NAME_BADGE_ID + " INTEGER PRIMARY KEY," +
                    Badge.BadgeEntry.COLUMN_NAME_BADGE_NAME+TEXT_TYPE+COMMA_SEP+
                    Badge.BadgeEntry.COLUMN_NAME_BADGE_DESC+TEXT_TYPE+COMMA_SEP+
                    Badge.BadgeEntry.COLUMN_NAME_BADGE_IMG+BLOB_TYPE+COMMA_SEP+
                    Badge.BadgeEntry.COLUMN_NAME_BADGE_DATE + INTEGER_TYPE +COMMA_SEP+
                    Badge.BadgeEntry.COLUMN_NAME_BADGE_SUBCATEGORY+TEXT_TYPE+COMMA_SEP+
                    Badge.BadgeEntry.COLUMN_NAME_BADGE_CATEGORY+TEXT_TYPE+");";

    /***********************************/

    private DevShackerDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static DevShackerDbHelper getInstance(Context ctx) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (mInstance == null) {
            mInstance = new DevShackerDbHelper(ctx.getApplicationContext());
        }
        return mInstance;
    }


    public static String getTableAsString(SQLiteDatabase db, String tableName) {
        Log.d("DBHELPER", "getTableAsString called");
        String tableString = String.format("Table %s:\n", tableName);
        Cursor allRows  = db.rawQuery("SELECT * FROM " + tableName, null);
        if (allRows.moveToFirst() ){
            String[] columnNames = allRows.getColumnNames();
            do {
                for (String name: columnNames) {
                    tableString += String.format("%s: %s\n", name,
                            allRows.getString(allRows.getColumnIndex(name)));
                }
                tableString += "\n";

            } while (allRows.moveToNext());
        }

        return tableString;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_CHALLENGE);
        db.execSQL(SQL_CREATE_CATEGORY);
        db.execSQL(SQL_CREATE_SUBCATEGORY);
        db.execSQL(SQL_CREATE_LEVELS);
        db.execSQL(SQL_CREATE_QUESTION);
        db.execSQL(SQL_CREATE_QUESTIONS_OF_CHALLENGE);
        db.execSQL(SQL_CREATE_USER);
        db.execSQL(SQL_CREATE_USERQUESTIONSRESULT);
        db.execSQL(SQL_CREATE_BADGE);

        Log.d("DataBaseOperations", "Data Base Created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_CREATE_CHALLENGE);
        db.execSQL(SQL_CREATE_CHALLENGE);
        db.execSQL(SQL_CREATE_CATEGORY);
        db.execSQL(SQL_CREATE_SUBCATEGORY);
        db.execSQL(SQL_CREATE_LEVELS);
        db.execSQL(SQL_CREATE_QUESTION);
        db.execSQL(SQL_CREATE_QUESTIONS_OF_CHALLENGE);
        db.execSQL(SQL_CREATE_USER);
        db.execSQL(SQL_CREATE_USERQUESTIONSRESULT);
        db.execSQL(SQL_CREATE_BADGE);

        onCreate(db);
    }
}
