package com.devteach.devshaker;

import android.app.Activity;
import android.content.ContentResolver;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.devteach.devshaker.model.DevShackerDbHelper;
import com.devteach.devshaker.model.Question;
import com.devteach.devshaker.utils.MenuActivityase;
import com.devteach.devshaker.utils.Utils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class QuestionTestDetailActivity extends MenuActivityase {

    private static ImageView imageQuestion;
    private static Button bA1;
    private static Button bA2;
    private static Button bA3;
    private static Button bA4;

    private static DevShackerDbHelper dbhelper;
    private static RelativeLayout layoutToListenSwipe;
    private static String [] userResponses = {};
    private static TextView questionText;
    private static boolean isfromtutorail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_question_test_detail, contentFrameLayout);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setVisibility(View.GONE);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(null);
        mToolbar.setNavigationOnClickListener(null);
        Bundle extras = getIntent().getExtras();
        dbhelper = DevShackerDbHelper.getInstance(this);
        String categoryName = extras.getString("categoryName");
        MenuActivityase.setToolbarColor(categoryName);
        MenuActivityase.setActivityTitlebyText("Details");

        int index = extras.getInt("index");
        String jsonArrayStr = extras.getString("userResponse");

        Log.d("A---->", index+"");
        Log.d("B---->", jsonArrayStr);
        JsonParser parser = new JsonParser();
        JsonArray jArray = parser.parse(jsonArrayStr).getAsJsonArray();
        JsonObject jsonObj = jArray.get(index).getAsJsonObject();
        long questionId = jsonObj.get("id").getAsLong();

        isfromtutorail = extras.getBoolean("IS_FROM_TUTORIAL");
        Question question = null;
        if(isfromtutorail){
            question = Utils.QUESTION_LIST_TUTORIAL.get(index);
        }else{
            question = Question.getQuestionById(questionId,dbhelper.getWritableDatabase());
        }
        //[{"id":488,"response":"1"},{"id":478,"response":"2"},{"id":481,"response":"1"},{"id":484,"response":"3"},{"id":477,"response":"2"}]
        String responses = jsonObj.get("response").getAsString();
        if(responses.contains("#")){
            userResponses = responses.split("#");
        }else{
            userResponses = new String[1];
            userResponses[0]=responses;
        }

            imageQuestion = (ImageView) findViewById(R.id.imageQuestion);
            bA1 = (Button) this.findViewById(R.id.buttonA1);
            bA2 = (Button) this.findViewById(R.id.buttonA2);
            bA3 = (Button) this.findViewById(R.id.buttonA3);
            bA4 = (Button) this.findViewById(R.id.buttonA4);
            questionText  = (TextView) this.findViewById(R.id.txtQuestion);

        setQuestionInfo(question,this ,userResponses);

    }

    public static void setQuestionInfo(Question q, Activity a,String [] userResponses) {
        Log.d("WIDTH QUESTIONTEXT",questionText.getLayoutParams().width+"");
        Log.d("QuestionTestActivity", "RespuestaCorrecta: " + q.getRightAnswers());
        String rawQuestionText = Html.fromHtml(q.getTextQuestion()).toString();
        //String lineSep = System.getProperty("line.separator");
        rawQuestionText= rawQuestionText.replaceAll("<br/>", "\n");


        questionText.setText(rawQuestionText);

        if (q.getImageQuestion() != null && q.getImageQuestion().trim().length() != 0) {
            Log.d("ImageURI", "ImageURI: " + q.getImageQuestion());
            Uri imageUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                        a.getResources().getResourcePackageName(R.drawable.q3img) + '/' +
                        a.getResources().getResourceTypeName(R.drawable.q3img) + '/' +
                        a.getResources().getResourceEntryName(R.drawable.q3img));

            System.out.println(imageUri.toString());
            imageQuestion.setImageURI(imageUri);
            imageQuestion.setVisibility(View.VISIBLE);
            questionText.setGravity(Gravity.NO_GRAVITY);
        } else {
            imageQuestion.setVisibility(View.INVISIBLE);
            questionText.setGravity(Gravity.CENTER);
            questionText.setGravity(Gravity.CENTER_VERTICAL);

        }

        bA1.setText(Html.fromHtml(q.getAnswer1()));
        bA2.setText(Html.fromHtml(q.getAnswer2()));
        bA3.setText(Html.fromHtml(q.getAnswer3()));
        bA4.setText(Html.fromHtml(q.getAnswer4()));



        String rightAnswes =  q.getRightAnswers();
        String rightAnswersArray [] = {};
        if(rightAnswes.contains("#")){
            rightAnswersArray = rightAnswes.split("#");
        }else{
            rightAnswersArray = new String[1];
            rightAnswersArray[0] = rightAnswes;
        }


        bA1.setActivated(false);
        bA1.setBackgroundResource(R.drawable.background);

        bA2.setActivated(false);
        bA2.setBackgroundResource(R.drawable.background);

        bA3.setActivated(false);
        bA3.setBackgroundResource(R.drawable.background);

        bA4.setActivated(false);
        bA4.setBackgroundResource(R.drawable.background);
        //0c8e0f

        //FF0000
        setArrayResponseAndColor(userResponses, R.drawable.background_bad);
        setArrayResponseAndColor(rightAnswersArray, R.drawable.background_correct);

    }


    private static void setArrayResponseAndColor(String arrayResponse[],int drawableResource){
        for(int i=0; i< arrayResponse.length; i++){
            int response = Integer.parseInt(arrayResponse[i]);
            switch (response){
                case 1:
                    bA1.setBackgroundResource(drawableResource);
                    break;
                case 2:
                    bA2.setBackgroundResource(drawableResource);
                    break;
                case 3:
                    bA3.setBackgroundResource(drawableResource);
                    break;
                case 4:
                    bA4.setBackgroundResource(drawableResource);
                    break;

                default:
                    System.out.println("OJETE "+response);
                    break;
            }
        }
    }


}

