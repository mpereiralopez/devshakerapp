package com.devteach.devshaker.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.devteach.devshaker.LandingActivity;
import com.devteach.devshaker.R;
import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ted on 27/12/2016.
 */

public class NotificationsListenerService extends GcmListenerService {

    @Override
    public void onMessageReceived(String s, Bundle bundle) {

        //System.out.println( bundle.getBundle("payload").get("data"));
        String myJSONString = bundle.getString("payload");

        try {
            JSONObject myJson = new JSONObject(myJSONString);
            System.out.println(myJson);
            System.out.println(myJson.getJSONObject("GCM"));
            System.out.println(myJson.getJSONObject("GCM").getJSONObject("data"));
            System.out.println(myJson.getJSONObject("GCM").getJSONObject("data").getString("message"));


            String message = myJson.getJSONObject("GCM").getJSONObject("data").getString("message");
            sendNotification(message);

        } catch (JSONException e) {
            e.printStackTrace();
        }





    }


    private void sendNotification(String message) {
        Intent intent = new Intent(this, LandingActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT
                | PendingIntent.FLAG_ONE_SHOT);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder nBuilder;
        nBuilder = new NotificationCompat.Builder(this)
                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(R.drawable.icon_app)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentTitle(getString(R.string.app_name))  // <--- add this
                .setContentText(message)   // <--- and this
                .setTicker(message)
                .setVibrate(new long[] { 100, 250, 100, 250, 100, 250 })
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setPriority(Notification.PRIORITY_MAX);


        NotificationManager mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, nBuilder.build());

    }
}
