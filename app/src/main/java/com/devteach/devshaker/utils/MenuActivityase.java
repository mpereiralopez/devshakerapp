package com.devteach.devshaker.utils;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.devteach.devshaker.BadgesActivity;
import com.devteach.devshaker.LandingActivity;
import com.devteach.devshaker.ProfileActivity;
import com.devteach.devshaker.R;
import com.devteach.devshaker.model.DevShackerDbHelper;
import com.devteach.devshaker.model.User;


public class MenuActivityase extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    ActionBarDrawerToggle toggle;
    private TextView userNameTextView;
    private TextView userEmailTextView;

    private static Toolbar toolbar;
    private static DevShackerDbHelper dbHelper ;
    private static Activity act;
    private static  ImageView imageView;
    private static Context ctx;

    private static DevShackerDbHelper dbhelper;
    private static User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        act = this;
        setContentView(R.layout.activity_landing);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setLogo(R.drawable.icon_app_white);

        //toolbar.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        //setSupportActionBar(toolbar);
        MenuActivityase.setToolbarColor("default");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);

        navigationView.setNavigationItemSelectedListener(this);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_EMAIL,  new String[]{"test@devshaker.com"});
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);
                String subject = "Feedback from: "+Build.MODEL+" "+  metrics.widthPixels+"x"+metrics.heightPixels+"d"+metrics.density+" OS"+Build.VERSION.RELEASE;
                intent.putExtra(Intent.EXTRA_SUBJECT, subject);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }

            }
        });

        //toggle.syncState();
        dbHelper = DevShackerDbHelper.getInstance(this);
        user = User.getUserFromDDBB(dbHelper.getReadableDatabase());
        if (user !=null){

            LinearLayout linearLayout = (LinearLayout) navigationView.getHeaderView(0);
            userNameTextView = (TextView)linearLayout.findViewById(R.id.menu_user_name);
            userEmailTextView = (TextView)linearLayout.findViewById(R.id.menu_user_email);
            userNameTextView.setText(user.getFirstName()+" "+user.getLastName());
            userEmailTextView.setText(user.getEmailAddress());
            ctx = getApplicationContext();
            imageView = (ImageView)linearLayout.findViewById(R.id.menu_user_profile_pic);
            Utils.loadImageFromStorage(imageView, user.getImg());

        }


    }

    public static void refreshMenuUserProfilePicture (Bitmap bitmap){

        if(bitmap == null)Utils.loadImageFromStorage(imageView,user.getImg());
            else imageView.setImageBitmap(bitmap);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        toggle.syncState();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.landing, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        System.out.println("ME VOY POR AQUI");
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();
        String messageBeta = "This is a beta version. Feature available soon";
        switch (id){
            case R.id.nav_home:
                Intent landingIntent = new Intent(this,LandingActivity.class);
                this.startActivity(landingIntent);
                break;
            case R.id.nav_profile:
                Intent profileIntent = new Intent(this,ProfileActivity.class);
                this.startActivity(profileIntent);
                break;

            /*case R.id.nav_friends:
                Snackbar.make(findViewById(R.id.drawer_layout), messageBeta, Snackbar.LENGTH_LONG).show();

                break;*/

            case R.id.nav_badges:
                Intent badgeIntebt = new Intent(this,BadgesActivity.class);
                this.startActivity(badgeIntebt);


                break;

            default:
                Snackbar.make(findViewById(R.id.drawer_layout), messageBeta, Snackbar.LENGTH_LONG).show();

                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static void setToolbarColor(String categoryName){

        int newColor = 0;

        if(categoryName.equalsIgnoreCase("code")){newColor= Color.parseColor(Utils.COLOR_CODE_HEX);}
        if(categoryName.equalsIgnoreCase("build")){newColor= Color.parseColor(Utils.COLOR_BUILD_HEX);}
        if(categoryName.equalsIgnoreCase("think")){newColor= Color.parseColor(Utils.COLOR_THINK_HEX);}
        if(categoryName.equalsIgnoreCase("default")){ newColor = Color.parseColor(Utils.COLOR_DEFAULT_HEX);}

        toolbar.setBackgroundColor(newColor);
        Window window = act.getWindow();

    // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

    // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

    // finally change the color
        if (Build.VERSION.SDK_INT >=  21) {
            window.setStatusBarColor(newColor);
        }

    }

    public static void setActivityTitlebyText(String title){
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(title);
    }

    public static void setActivityTitlebyResource(int titleResource){
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(titleResource);
    }




}
