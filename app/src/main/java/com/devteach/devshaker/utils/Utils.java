package com.devteach.devshaker.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.devteach.devshaker.LandingActivity;
import com.devteach.devshaker.R;
import com.devteach.devshaker.model.Badge;
import com.devteach.devshaker.model.Question;
import com.devteach.devshaker.model.User;
import com.google.gson.Gson;
import com.liferay.mobile.android.auth.basic.BasicAuthentication;
import com.liferay.mobile.android.service.Session;
import com.liferay.mobile.android.service.SessionImpl;
import com.liferay.mobile.android.util.PortraitUtil;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by mpereira on 11/4/15.
 */
public class Utils {

    public static final int answerStatusRadious = 60;
    public static final int codeLanguageRadious = 80;

    public static final String PROFILE_PIC_FILE_FOLDER = "profilePics";

    public static final String LEVEL_COMMPLETED_KEY = "levelCompleted";
    public static final String CODE_LANDING_KEY = "codeLanding";

    //public static final String URL_BASE= "http://192.168.1.35:8080/";
    public static final String URL_BASE= "http://62.174.82.57/";
    public static final String DEFAULT_APK_ACCOUNT_EMAIL = "android@apk.com";
    public static final String DEFAULT_APK_ACCOUNT_PASS = "androidApk2016?";
    public static final long DEFAULT_COMPANY_ID = 20116;

    public static final String COLOR_CODE_HEX = "#1A7890";
    public static final String COLOR_BUILD_HEX = "#132589";
    public static final String COLOR_THINK_HEX = "#636B69";
    public static final String COLOR_DEFAULT_HEX = "#57aed5";

    public final static int PROFILE_PIC_MAX_WH = 275;

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public static final LinkedHashMap<String,String> COUNTRY_AC3_MAP ;
    static{
        COUNTRY_AC3_MAP =  new LinkedHashMap<String, String>();
        COUNTRY_AC3_MAP.put("España","ESP");
        COUNTRY_AC3_MAP.put("United Kingdom","GBR");
        COUNTRY_AC3_MAP.put("France","FRA");
    }

    public static final LinkedList<Question> QUESTION_LIST_TUTORIAL;
    static {
        QUESTION_LIST_TUTORIAL = new LinkedList<Question>();
        Question q1 = new Question(0, 0, "Which one of this operator is used to allocate memory?", null, "memory", "delete", "new", "allocate", "3", 0);
        Question q2 = new Question(0, 0, "Which language does not need to be compiled?", null, "Javascript", "C", "C++", "Fortran", "1", 0);
        Question q3 = new Question(0, 0, "What is the output of the program?", "@drawable/q3img", "print() is called 10 times", "x = 10 y=10", "; after if(x!=y) produces error", "No output",
                "3", 0);
        QUESTION_LIST_TUTORIAL.add(q1);
        QUESTION_LIST_TUTORIAL.add(q3);
        QUESTION_LIST_TUTORIAL.add(q2);
    }

    public static TextView createResponseCircle (Context context,Resources resources,HashMap<String,Object> mapa, String text,int h){

        float ht_px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, h, resources.getDisplayMetrics());
        TextView cloned = new TextView(context);
        cloned.setGravity(Gravity.CENTER);
        cloned.setTextColor(Color.WHITE);
        cloned.setTextSize(18);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Math.round(ht_px),Math.round(ht_px));
        params.setMargins(15, 0, 5, 0); //substitute parameters for left, top, right, bottom
        cloned.setLayoutParams(params);
        cloned.setText(text);
        int id = (int) mapa.get("index");
        cloned.setId(id);


        if(mapa.containsKey(LEVEL_COMMPLETED_KEY)){
            boolean correct = ((Boolean) mapa.get(LEVEL_COMMPLETED_KEY)).booleanValue();
            if (correct) {
                cloned.setBackgroundDrawable(resources.getDrawable(R.drawable.circle_green));
            } else{
                cloned.setBackgroundDrawable(resources.getDrawable(R.drawable.circle_red));
            }
        }

        if(mapa.containsKey(CODE_LANDING_KEY)){
            cloned.setBackgroundDrawable(resources.getDrawable(R.drawable.circle_code));
        }
        return cloned;
    }


    public static boolean haveNetworkConnection(Context ctx) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

























    public static void downloadImageFromServer(String url,final SQLiteDatabase db, final Object obj,final Context ctx){
        BasicImageDownloader downloader = new BasicImageDownloader(new BasicImageDownloader.OnImageLoaderListener() {
            @Override
            public void onError(BasicImageDownloader.ImageError error) {
                Log.d("BasicImageDownloader",error.toString());
            }

            @Override
            public void onProgressChange(int percent) {
                Log.d("onProgressChange",percent+"%");
            }

            @Override
            public void onComplete(Bitmap result) {

                final Bitmap.CompressFormat mFormat = Bitmap.CompressFormat.PNG;
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                result.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);

                String fileName = new String();
                String folderName = new String();

                 if(obj instanceof Badge){
                     fileName = "badge"+System.currentTimeMillis()+".png";
                     folderName = "badges";
                 }

                if(obj instanceof User){
                    fileName = "userPic"+System.currentTimeMillis()+".png";
                    folderName = Utils.PROFILE_PIC_FILE_FOLDER;
                }

                saveBitmapToLocalDisk(result,obj,ctx,db,fileName,folderName,null);

            }
        });
        downloader.download(url, true);
    }



    public static void saveBitmapToLocalDisk(final Bitmap result, final Object obj, final Context ctx, final SQLiteDatabase db, final String fileName, String folderName, final ImageView destinyImageView){
        final Bitmap.CompressFormat mFormat = Bitmap.CompressFormat.PNG;

        ContextWrapper cw = new ContextWrapper(ctx.getApplicationContext());
        File directory = cw.getDir(folderName, Context.MODE_PRIVATE);

        final File myImageFile = new File(directory,fileName);


        BasicImageDownloader.writeToDisk(myImageFile, result, new BasicImageDownloader.OnBitmapSaveListener() {
            @Override
            public void onBitmapSaved() {
                Log.d("SAVING IMAGE", "Image saved as: " + myImageFile.getAbsolutePath());

                if(obj instanceof User){
                    User u = (User) obj;
                    User.updateUserProfilePic(u.getUserId(),myImageFile.getAbsolutePath(),db);
                    if(destinyImageView!=null){
                        destinyImageView.setImageBitmap(result);
                        //Utils.loadImageFromStorage(destinyImageView,u.getImg());
                        MenuActivityase.refreshMenuUserProfilePicture(result);
                    }
                }

                if(obj instanceof  Badge){
                    Badge badge = (Badge) obj;
                    badge.setImg(myImageFile.getAbsolutePath());
                    Badge.saveBadgeToDDBB(badge,db);
                }


            }

            @Override
            public void onBitmapSaveError(BasicImageDownloader.ImageError error) {
                Log.d("SAVING IMAGE", "Error code " + error.getErrorCode() + ": " +
                        error.getMessage());
                error.printStackTrace();
            }


        }, mFormat, true);
    }


   public static void loadImageFromStorage(ImageView view, String path)
    {

        Bitmap b = decodeSampledBitmapFromFile(path);
        view.setImageBitmap(b);
    }


    public static Bitmap scaleBitmap(Bitmap bitmap, int wantedWidth, int wantedHeight) {
        Bitmap output = Bitmap.createBitmap(wantedWidth, wantedHeight, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(output);
        Matrix m = new Matrix();
        m.setScale((float) wantedWidth / bitmap.getWidth(), (float) wantedHeight / bitmap.getHeight());
        canvas.drawBitmap(bitmap, m, new Paint());

        return output;
    }



    public static Bitmap decodeSampledBitmapFromFile(String path) {

        // First decode with inJustDecodeBounds=true to check dimensions
        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > PROFILE_PIC_MAX_WH)
        {
            inSampleSize = Math.round((float)height / (float)PROFILE_PIC_MAX_WH);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > PROFILE_PIC_MAX_WH)
        {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float)width / (float)PROFILE_PIC_MAX_WH);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    public static void setUserInfoAfterLoginOrSingUp(JSONObject result,SQLiteDatabase db,String clearPassword,final Context ctx, final Intent intent, boolean isNewUser){
        User u = null;
        User userPrev = User.getUserFromDDBB(db);
        try {
            long contactId = result.getLong("contactId");
            Gson g = new Gson();
            u = g.fromJson(result.toString(),User.class);
            u.setPassword(clearPassword);
            if(userPrev == null){
                User.saveUserToDDBB(u,db);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        getUserInformation(u,db,ctx,intent,isNewUser);

    }

    private static void getUserInformation(User user, SQLiteDatabase db,final Context ctx, final Intent intent,boolean isNewUser){
        Session sessionUserInfo = new SessionImpl(Utils.URL_BASE,new BasicAuthentication(user.getEmailAddress(),user.getPassword()));
        try {
            boolean isMale = (user.isMale() == 1);
            String portraitURL = PortraitUtil.getPortraitURL(sessionUserInfo,isMale,user.getPortraitId(),user.getUuid());
            downloadImageFromServer(portraitURL,db,user,ctx);
        } catch (Exception e) {
            e.printStackTrace();
        }
        goToCategoryLanding(ctx);
    }

    private static void goToCategoryLanding(Context ctx){
        Intent mainIntent = new Intent(ctx,LandingActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ctx.startActivity(mainIntent);
    }


    public static void showSortPopup(final Activity context, final List<Badge> listaBadges) {
        // Inflate the popup_layout.xml
        FrameLayout viewGroup = (FrameLayout) context.findViewById(R.id.level_complete_layout); //Remember this is the FrameLayout area within your activity_main.xml
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.pop_up_badge, viewGroup, false);

        TextView subcat_pup_up = (TextView) layout.findViewById(R.id.subcat_pup_up);
        ImageView pop_up_image_badge = (ImageView) layout.findViewById(R.id.pop_up_image_badge);
        TextView pop_up_badge_title = (TextView) layout.findViewById(R.id.pop_up_badge_title);
        TextView pop_up_badge_description = (TextView) layout.findViewById(R.id.pop_up_badge_description);

        subcat_pup_up.setText(listaBadges.get(0).getSubcategoryName());

        pop_up_badge_title.setText(listaBadges.get(0).getName());
        pop_up_badge_description.setText(listaBadges.get(0).getDescription());

        // Creating the PopupWindow
        final PopupWindow changeSortPopUp = new PopupWindow(context);
        changeSortPopUp.setBackgroundDrawable(null);
        changeSortPopUp.setContentView(layout);
        changeSortPopUp.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        changeSortPopUp.setHeight(LinearLayout.LayoutParams.MATCH_PARENT);
        changeSortPopUp.setFocusable(true);
        pop_up_image_badge.setImageBitmap(decodeSampledBitmapFromFile(listaBadges.get(0).getImg()));

        // Clear the default translucent background
        //Point p = new Point(60,60);
        // Displaying the popup at the specified location, + offsets.
        changeSortPopUp.showAtLocation(layout, Gravity.CENTER, 0, 0);


        // Getting a reference to Close button, and close the popup when clicked.
        Button close = (Button) layout.findViewById(R.id.close_pop_up);
        close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                listaBadges.remove(0);
                if (listaBadges.isEmpty()) {
                    changeSortPopUp.dismiss();
                } else {
                    changeSortPopUp.dismiss();
                    showSortPopup(context, listaBadges);
                }
            }
        });

    }


   /* private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        Badge badge;

        public DownloadImageTask(ImageView bmImage,Badge badge) {
            this.bmImage = bmImage;
            this.badge = badge;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            if(urldisplay.indexOf("http")==-1){
                urldisplay = Utils.URL_BASE+"/"+urldisplay;
            }
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {

            bmImage.setImageBitmap(result);

            final Bitmap.CompressFormat mFormat = Bitmap.CompressFormat.PNG;

            ContextWrapper cw = new ContextWrapper(ctx.getApplicationContext());
            File directory = cw.getDir("badges", Context.MODE_PRIVATE);

            final File myImageFile = new File(directory,"badge"+System.currentTimeMillis()+".png");


            BasicImageDownloader.writeToDisk(myImageFile, result, new BasicImageDownloader.OnBitmapSaveListener() {
                @Override
                public void onBitmapSaved() {
                    Log.d("SAVING IMAGE", "Image saved as: " + myImageFile.getAbsolutePath());
                    badge.setImg(myImageFile.getAbsolutePath());
                    Badge.saveBadgeToDDBB(badge,dbhelper.getWritableDatabase());
                }

                @Override
                public void onBitmapSaveError(BasicImageDownloader.ImageError error) {
                    Log.d("SAVING IMAGE", "Error code " + error.getErrorCode() + ": " +
                            error.getMessage());
                    error.printStackTrace();
                }


            }, mFormat, true);

        }
    }*/



}
