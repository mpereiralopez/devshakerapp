package com.devteach.devshaker.utils;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.devteach.devshaker.R;
import com.devteach.devshaker.model.DevShackerDbHelper;
import com.devteach.devshaker.model.User;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.liferay.mobile.android.auth.basic.BasicAuthentication;
import com.liferay.mobile.android.callback.Callback;
import com.liferay.mobile.android.callback.typed.JSONObjectCallback;
import com.liferay.mobile.android.service.Session;
import com.liferay.mobile.android.service.SessionImpl;
import com.liferay.mobile.android.v7.pushnotificationsdevice.PushNotificationsDeviceService;

import org.json.JSONObject;

import java.io.IOException;


/**
 * Created by ted on 27/12/2016.
 */

public class RegistrationService extends IntentService {

    public RegistrationService() {
        super("RegistrationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        InstanceID myID = InstanceID.getInstance(this);
        String registrationToken = null;
        System.out.println("--------------------------------------------------------------------"+getString(R.string.gcm_defaultSenderId));
        try {
            registrationToken = myID.getToken(
                    getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE,
                    null
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d("Registration Token", registrationToken);
        DevShackerDbHelper dbhelper = DevShackerDbHelper.getInstance(this);
        User user = User.getUserFromDDBB(dbhelper.getReadableDatabase());

        if(user!=null){
            System.out.println(user.getEmailAddress());
            System.out.println(user.getPassword());

            Session sessionForPush = new SessionImpl(Utils.URL_BASE,new BasicAuthentication(user.getEmailAddress(),user.getPassword()));
            PushNotificationsDeviceService pushServices = new PushNotificationsDeviceService(sessionForPush);
            try {
                Callback callbackPush = new JSONObjectCallback() {
                    @Override
                    public void onFailure(Exception exception) {
                        Log.d("PUSH","FAILED TO REGISTER");
                        System.out.println(exception);
                    }

                    @Override
                    public void onSuccess(JSONObject result) {
                        Log.d("PUSH","SUCESS");
                        System.out.println(result);
                    }
                };
                sessionForPush.setCallback(callbackPush);



                pushServices.addPushNotificationsDevice(registrationToken,"Android");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
