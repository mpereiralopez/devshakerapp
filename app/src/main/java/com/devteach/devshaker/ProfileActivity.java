package com.devteach.devshaker;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.system.ErrnoException;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.devteach.devshaker.model.DevShackerDbHelper;
import com.devteach.devshaker.model.User;
import com.devteach.devshaker.utils.MenuActivityase;
import com.devteach.devshaker.utils.Utils;
import com.liferay.mobile.android.auth.basic.BasicAuthentication;
import com.liferay.mobile.android.callback.Callback;
import com.liferay.mobile.android.callback.typed.JSONObjectCallback;
import com.liferay.mobile.android.service.Session;
import com.liferay.mobile.android.service.SessionImpl;
import com.liferay.mobile.android.v7.api.ApiService;
import com.liferay.mobile.android.v7.user.UserService;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 */
public class ProfileActivity extends MenuActivityase {



    private static final int IMAGE_SELECTED = 200;
    private static User user;
    private final static int REQUEST_CAMERA = 0;
    private final static int SELECT_FILE = 1;
    private SQLiteDatabase db;
    private static ImageView profilePictureView;
    private EditText birthdate;
    private TextView profilePicName;
    private EditText displayName;
    private EditText name;
    private EditText email;
    private EditText phone;
    private String aC3CountryValue;
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private ProgressDialog progress;
    private static DevShackerDbHelper dbhelper;
    private CropImageView mCropImageView;
    private Uri mCropImageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_profile);
        dbhelper = DevShackerDbHelper.getInstance(this);


        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_profile, contentFrameLayout);



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setVisibility(View.GONE);

        user = User.getUserFromDDBB(dbhelper.getReadableDatabase());

        profilePictureView = (ImageView) findViewById(R.id.profile_circle_pic);
        Utils.loadImageFromStorage(profilePictureView,user.getImg());


        MenuActivityase.setToolbarColor("default");
        MenuActivityase.setActivityTitlebyResource(R.string.menu_profile);


        profilePicName = (TextView) findViewById(R.id.profile_pic_name);
        displayName = (EditText)findViewById(R.id.profile_display_name);
        name = (EditText)findViewById(R.id.profile_name);
        email = (EditText)findViewById(R.id.profile_email);
        phone = (EditText)findViewById(R.id.profile_phone);
        birthdate = (EditText)findViewById(R.id.profile_birthdate);

        profilePicName.setText(user.getScreenName());
        displayName.setText(user.getScreenName());
        name.setText(user.getFirstName());
        email.setText(user.getEmailAddress());
        phone.setText(user.getPhone());
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(user.getBirthdate());
        birthdate.setText(simpleDateFormat.format(calendar.getTime()));

        String ac3CountryCode = (user.getCountry()!=null)?user.getCountry():"";
        Log.d("ac3CountryCode-->",ac3CountryCode);

        Spinner spinner = (Spinner) findViewById(R.id.spinner_countries);
        spinner.setPrompt("Select a country");

        if(ac3CountryCode!=null && ac3CountryCode.length()==3){
            int pos=0,i = 0;
            Iterator<String> it = Utils.COUNTRY_AC3_MAP.values().iterator();
            while(it.hasNext()){
                if(it.next().equals(ac3CountryCode))pos =i;
                i++;
            }
                spinner.setSelection(pos);
            }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d("CountrySelected ","i= "+i+" long "+l);

                aC3CountryValue =  Utils.COUNTRY_AC3_MAP.get(adapterView.getItemAtPosition(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    /**
     * Crop the image and set it back to the  cropping view.
     */
    public void onCropImageClick(View view) {
        Bitmap cropped = mCropImageView.getCroppedImage();
        if (cropped != null) {
            System.out.println(cropped.getWidth());
            System.out.println(cropped.getHeight());
            //Utils.decodeSampledBitmapFromFile(cropped,300,300);

            Bitmap scaledBitmap = Utils.scaleBitmap(cropped, Utils.PROFILE_PIC_MAX_WH,Utils.PROFILE_PIC_MAX_WH);

            System.out.println(scaledBitmap.getWidth());
            System.out.println(scaledBitmap.getHeight());
            System.out.println("TOTAL BYTES AFTER CHANGE "+scaledBitmap.getByteCount());

            String fileName = "userPic"+System.currentTimeMillis()+".png";

            Utils.saveBitmapToLocalDisk(scaledBitmap,user,this.getApplicationContext(),dbhelper.getWritableDatabase(),fileName,Utils.PROFILE_PIC_FILE_FOLDER,profilePictureView);
        }

        RelativeLayout CropContainer = (RelativeLayout) findViewById(R.id.crop_container);
        CropContainer.setEnabled(false);
        CropContainer.setVisibility(View.INVISIBLE);


    }



    public void selectImage(View v) {
        Utils.verifyStoragePermissions(ProfileActivity.this);
        startActivityForResult(getPickImageChooserIntent(), IMAGE_SELECTED);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {

            Uri imageUri =  getPickImageResultUri(data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage,
            // but we don't know if we need to for the URI so the simplest is to try open the stream and see if we get error.
            boolean requirePermissions = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    isUriRequiresPermissions(imageUri)) {

                // request permissions and handle the result in onRequestPermissionsResult()
                requirePermissions = true;
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            }

            if (!requirePermissions) {

                /*FrameLayout item = (FrameLayout)findViewById(R.id.profile_root);
                View child = getLayoutInflater().inflate(R.layout.crop_image_inflate,null);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                params.setMargins(15,15,15,15);
                child.setLayoutParams(params);*/
                mCropImageView = (CropImageView) findViewById(R.id.CropImageView);
                //mCropImageView.setAspectRatio(5, 10);
                mCropImageView.setFixedAspectRatio(true);
                mCropImageView.setGuidelines( CropImageView.Guidelines.ON);
                mCropImageView.setCropShape(CropImageView.CropShape.OVAL);
                mCropImageView.setScaleType(CropImageView.ScaleType.FIT_CENTER);
                //mCropImageView.setAutoZoomEnabled(true);
                mCropImageView.setShowProgressBar(true);
                //mCropImageView.setCropRect(new Rect(0, 0, 800, 500));
                mCropImageView.setShowCropOverlay(true);
                //item.addView(child);
                mCropImageView.setImageUriAsync(imageUri);
                RelativeLayout CropContainer = (RelativeLayout) findViewById(R.id.crop_container);
                CropContainer.bringToFront();
                CropContainer.setVisibility(View.VISIBLE);
            }
        }

    }



    public void popupCalendar(View view) {
        try {
            // We need to get the instance of the LayoutInflater
            System.out.println("DENTRO DE POPUP CALENDAR");


            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            final DatePicker picker = new DatePicker(this);
            picker.setCalendarViewShown(false);
            Calendar cal = Calendar.getInstance();
            picker.setMaxDate(cal.getTime().getTime());
            cal.set(1970,1,1);
            picker.setMinDate(cal.getTime().getTime());
            builder.setView(picker);
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.setPositiveButton("Set", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.d("CALENDAR ",which+"");
                    //mCalendar.set(mDatePicker.getYear(), mDatePicker.getMonth(), mDatePicker.getDayOfMonth());
                    Calendar cal = Calendar.getInstance();
                    cal.set(picker.getYear(),picker.getMonth(),picker.getDayOfMonth());
                    birthdate.setText(simpleDateFormat.format(cal.getTime()));

                }
            });
            builder.show();
    } catch (Exception e) {
        e.printStackTrace();
    }

}


    public void uploadUserInfoToServer(View view){
        Log.d("Profile Activity","UPLOADING USER INFO TU SERVER");


        /*Show dialog*/
        progress = ProgressDialog.show(this, "Saving",
                "Saving data to Server, please wait", true);

        String screenName = this.displayName.getText().toString();
        String name = this.name.getText().toString();
        String email = this.email.getText().toString();
        final String phone = this.phone.getText().toString();
        final String birthDate = this.birthdate.getText().toString();
        Date dateOfBirth = null;
        try {
             dateOfBirth = simpleDateFormat.parse(birthDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        final User u = User.getUserFromDDBB(dbhelper.getReadableDatabase());

        Bitmap thumbnail = Utils.decodeSampledBitmapFromFile(u.getImg());
        int bytes = thumbnail.getByteCount();
        System.out.println("------------------------> BYTES TO SEND "+bytes);
        System.out.println("------------------------>SIZE TO SEND "+thumbnail.getWidth()+" "+thumbnail.getHeight());
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        thumbnail.compress(Bitmap.CompressFormat.PNG, 100, out);
        /*ByteBuffer buffer = ByteBuffer.allocate(bytes); //Create a new buffer
        thumbnail.copyPixelsToBuffer(buffer);*/
        final byte[] byteArray =  out.toByteArray();


        Session session = new SessionImpl(Utils.URL_BASE,new BasicAuthentication(u.getEmailAddress(),u.getPassword()));

        ApiService apiService = new ApiService(session);

        session.setCallback(new JSONObjectCallback() {
            @Override
            public void onFailure(Exception exception) {
                Log.d("WS UPDATE USER ", exception.toString());
                String message = "Error uploading user information. Please check information and try again";
                progress.dismiss();
                Snackbar.make(findViewById(R.id.drawer_layout), message, Snackbar.LENGTH_LONG).show();

            }

            @Override
            public void onSuccess(JSONObject result) {
                Log.d("WS UPDATE USER ",result.toString());

                long time = 0;
                try {
                    time = simpleDateFormat.parse(birthDate).getTime();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                u.setBirthdate(time);
                u.setCountry(aC3CountryValue);
                u.setPhone(phone);

                User.saveUserToDDBB(u,dbhelper.getWritableDatabase());

                Session sessionUploadProfile = new SessionImpl(Utils.URL_BASE,new BasicAuthentication(u.getEmailAddress(),u.getPassword()));
                UserService userServiceUploadProfile = new UserService(sessionUploadProfile);
                Callback userUpdatedProfileCallBack = new JSONObjectCallback() {
                    @Override
                    public void onFailure(Exception e) {
                        Log.d("UPDATE PICTURE ERROR", e.toString());
                        String message = "Error uploading picture, try other";
                        Snackbar.make(findViewById(R.id.drawer_layout), message, Snackbar.LENGTH_LONG).show();
                        progress.dismiss();

                    }

                    @Override
                    public void onSuccess(JSONObject jsonObject) {
                        Log.d("UPDATE PICTURE OK", jsonObject.toString());
                        String message = "Profile updated correctly";
                        progress.dismiss();
                        Snackbar.make(findViewById(R.id.drawer_layout), message, Snackbar.LENGTH_LONG).show();
                        MenuActivityase.refreshMenuUserProfilePicture(null);
                    }
                };
                try {
                    sessionUploadProfile.setCallback(userUpdatedProfileCallBack);
                    userServiceUploadProfile.updatePortrait(u.getUserId(),byteArray);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        try {
            apiService.updateUserInfo(user.getUserId(),screenName,name,email,phone,dateOfBirth.getYear(),dateOfBirth.getMonth(),dateOfBirth.getDate(),aC3CountryValue);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * Test if we can open the given Android URI to test if permission required error is thrown.<br>
     */
    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (e.getCause() instanceof ErrnoException) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    /**
     * Get the URI of the selected image from  {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera  and gallery image.
     *
     * @param data the returned data of the  activity result
     */
    public Uri getPickImageResultUri(Intent  data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null  && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ?  getCaptureImageOutputUri() : data.getData();
    }


    /**
     * Create a chooser intent to select the  source to get image from.<br/>
     * The source can be camera's  (ACTION_IMAGE_CAPTURE) or gallery's (ACTION_GET_CONTENT).<br/>
     * All possible sources are added to the  intent chooser.
     */
    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to  save.
        Uri outputFileUri =  getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager =  getPackageManager();

        // collect all camera intents
        Intent captureIntent = new  Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam =  packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new  Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new  Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery =  packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new  Intent(galleryIntent);
            intent.setComponent(new  ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the  list (fucking android) so pickup the useless one
        Intent mainIntent =  allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if  (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity"))  {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main  intent
        Intent chooserIntent =  Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,  allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture  by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new  File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

}