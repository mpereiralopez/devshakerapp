package com.devteach.devshaker;

import android.content.Intent;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.graphics.ColorUtils;
import android.view.View;

import com.devteach.devshaker.slides.FirstSlide;
import com.devteach.devshaker.slides.SecondSlide;
import com.devteach.devshaker.slides.ThirdSlide;
import com.github.paolorotolo.appintro.AppIntro;

public class TutorialActivity extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Add your slide's fragments here.
        // AppIntro will automatically generate the dots indicator and buttons.
        //addSlide(getSupportFragmentManager().findFragmentById(R.id.tutorial_fragment1));
        addSlide(new FirstSlide());
        addSlide(new SecondSlide());
        addSlide(new ThirdSlide());


        setTitle("");
        setBarColor(ColorUtils.setAlphaComponent(getResources().getColor(R.color.primary),0));
        setSeparatorColor(ColorUtils.setAlphaComponent(getResources().getColor(R.color.primary),0));
        setIndicatorColor(getResources().getColor(R.color.primary),getResources().getColor(R.color.primary_light));
        setColorDoneText(getResources().getColor(R.color.primary));

        // Hide Skip/Done button.
        showSkipButton(false);
        setProgressButtonEnabled(true);

        // Turn vibration on and set intensity.
        // NOTE: you will probably need to ask VIBRATE permisssion in Manifest.
        setVibrate(true);
        setVibrateIntensity(30);
        setFadeAnimation();

    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.
        startDemo(currentFragment.getView());
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        // Do something when users tap on Done button.
        startDemo(currentFragment.getView());

    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }

    public void startDemo(View view){
        //System.out.println("Button pressed");
        //Aqui lanzo las preguntas
        Intent mainIntent = new Intent(TutorialActivity.this,QuestionTestActivity.class);
        mainIntent.putExtra("IS_FROM_TUTORIAL", true);

        TutorialActivity.this.startActivity(mainIntent);
        TutorialActivity.this.finish();
    }
}