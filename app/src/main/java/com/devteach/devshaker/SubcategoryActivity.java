package com.devteach.devshaker;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.devteach.devshaker.model.Category;
import com.devteach.devshaker.model.DevShackerDbHelper;
import com.devteach.devshaker.model.Level;
import com.devteach.devshaker.model.Subcategory;
import com.devteach.devshaker.utils.MenuActivityase;

import java.util.Iterator;
import java.util.List;



/**
 * Created by mpereira on 11/4/15.
 */
public class SubcategoryActivity extends MenuActivityase implements View.OnClickListener {

    private static final String TAG= "CODELANDING.java";
    private static List<Subcategory> subcategoryList;
    private static DevShackerDbHelper dbhelper;
    private static TableLayout tableLayout;
    private static Context ctx;
    private static SubcategoryActivity subcategoryActivity;
    private static ProgressDialog progress;
    private static ProgressBar mProgress;
    private static String categoryName = new String();

    private long catId=0;
    //LevelCompleted levelCompleted = new LevelCompleted(int percentageComplete, );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_subcategories, contentFrameLayout);
        tableLayout =(TableLayout)findViewById(R.id.code_table);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            catId = extras.getLong("catId");
        }
        dbhelper = DevShackerDbHelper.getInstance(this);
        Category category = Category.getCategoryFromDDBB(dbhelper.getReadableDatabase(),catId);
        subcategoryList =  Subcategory.getListSubcategoryFromDB(category.getId(), dbhelper.getReadableDatabase());


        //1A7890
        String hexColor = new String();
        categoryName = category.getName();

        MenuActivityase.setToolbarColor(categoryName);
        MenuActivityase.setActivityTitlebyText(category.getName());
        ctx = this;
        setSubcategoryLayout(ctx, this);

    }

    private static void  setSubcategoryLayout( Context ctx, SubcategoryActivity act){
        Iterator<Subcategory> it = subcategoryList.iterator();
        int index = 0;
        int rowCounter = 0;
        TableRow row=null ;
        int resto = subcategoryList.size()%3;

        while(it.hasNext()){
            Log.d(TAG,"INDEX: "+index);
            Subcategory sub = it.next();
            if(index%3 == 0){
                row = new TableRow(ctx);
                row.setId(index);
                row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));
                tableLayout.addView(row,new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
                rowCounter++;
                if(rowCounter == 0 || rowCounter%2==0){
                    moveRtL(row);
                }else{
                    moveLtR(row);
                }
            }

            index = index+1;

            LinearLayout linearLayout = new LinearLayout(ctx);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams params = new TableRow.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight=1;
            linearLayout.setLayoutParams(params);



            Button btn = new Button(ctx);
            btn.setId((int) sub.getId());
            if(categoryName.equalsIgnoreCase("code")){ btn.setBackgroundDrawable(ctx.getResources().getDrawable(R.drawable.circle_code));}
            if(categoryName.equalsIgnoreCase("build")){ btn.setBackgroundDrawable(ctx.getResources().getDrawable(R.drawable.circle_build));}
            if(categoryName.equalsIgnoreCase("think")){ btn.setBackgroundDrawable(ctx.getResources().getDrawable(R.drawable.circle_think));}

            TableRow.LayoutParams btnLP = new TableRow.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);

            btnLP.gravity= Gravity.CENTER;
            btnLP.setMargins(0, 40, 0, 0);
            btn.setId((int) sub.getId());
            btn.setLayoutParams(btnLP);
            btn.setMaxWidth(120);
            btn.setMaxHeight(120);
            btn.setText(sub.getName());
            btn.setTextColor(Color.WHITE);



            btn.setOnClickListener(act);


            TextView textview = new TextView(ctx);

            TableRow.LayoutParams textViewLP =   new TableRow.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            textview.setGravity(Gravity.CENTER);
            textview.setLayoutParams(textViewLP);

            textview.setText(Level.getLevelProgressInfoForSubcategoryActivity(sub.getId(),dbhelper.getReadableDatabase()));

            textview.setTextSize(16);
            //moveRtL(btn);


            linearLayout.addView(btn);
            linearLayout.addView(textview);
            row.addView(linearLayout);


        }



        //Relleno con vacios
        if(resto>0){
            for(int i=resto; i<3;i++){
                LinearLayout linearLayout = new LinearLayout(ctx);
                linearLayout.setOrientation(LinearLayout.VERTICAL);
                TableRow.LayoutParams params = new TableRow.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
                params.weight=1;
                linearLayout.setLayoutParams(params);
                row.addView(linearLayout);
            }
        }

    }

    private static  void moveRtL(View view){

        Animation animation1 = AnimationUtils.loadAnimation(ctx, R.anim.move_r_to_l);
        view.startAnimation(animation1);
    }

    private static  void moveLtR(View view){
        Animation animation2 = AnimationUtils.loadAnimation(ctx, R.anim.move_l_to_r);
        view.startAnimation(animation2);
    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        Subcategory subcategorySelected = null;
        for(int i=0; i<subcategoryList.size();i++){
            if(subcategoryList.get(i).getId() == v.getId()){
                subcategorySelected =subcategoryList.get(i);
                break;
            }
        }
        System.out.println(subcategorySelected.getName());
        Intent mainIntent = new Intent(SubcategoryActivity.this,LevelActivity.class);
        mainIntent.putExtra("Subcategory",subcategorySelected);
        SubcategoryActivity.this.startActivity(mainIntent);

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent mainIntent = new Intent(SubcategoryActivity.this,LandingActivity.class);
        SubcategoryActivity.this.startActivity(mainIntent);
    }

}