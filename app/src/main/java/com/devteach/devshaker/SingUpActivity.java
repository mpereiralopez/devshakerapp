package com.devteach.devshaker;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.devteach.devshaker.model.DevShackerDbHelper;
import com.devteach.devshaker.utils.Utils;
import com.liferay.mobile.android.auth.basic.BasicAuthentication;
import com.liferay.mobile.android.callback.Callback;
import com.liferay.mobile.android.callback.typed.JSONObjectCallback;
import com.liferay.mobile.android.service.Session;
import com.liferay.mobile.android.service.SessionImpl;
import com.liferay.mobile.android.v7.user.UserService;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;


/**
 * A login screen that offers login via email/password.
 */
public class SingUpActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private static boolean mAuthTask = false;

    // UI references.
    private static AutoCompleteTextView mEmailView;
    private static EditText mPasswordView;
    private static EditText mPasswordRepeatedView;
    private static EditText mDisplayName;
    private static EditText mFullName;
    private static EditText mSurname;

    private static View mProgressView;
    private static View mLoginFormView;

    private static DevShackerDbHelper dbhelper;
    private static Activity act;
    private static Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_up);
        dbhelper = DevShackerDbHelper.getInstance(this);

        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();
        ctx = this;
        act = this;

        mDisplayName = (EditText)findViewById(R.id.display_name);
        mFullName = (EditText)findViewById(R.id.name);
        mSurname = (EditText)findViewById(R.id.surname);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptSingUp();
                    return true;
                }
                return false;
            }
        });

        mPasswordRepeatedView = (EditText) findViewById(R.id.repeated_password);

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptSingUp();
            }
        });

        mLoginFormView = findViewById(R.id.singup_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    private void populateAutoComplete() {
        getLoaderManager().initLoader(0, null, this);
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptSingUp() {
        if (mAuthTask != false) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        final String password = mPasswordView.getText().toString();
        String repeated_password = mPasswordRepeatedView.getText().toString();
        String displayName = mDisplayName.getText().toString();
        String name = mFullName.getText().toString();
        String lastName = mSurname.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if(!TextUtils.equals(password,repeated_password)){
            mPasswordView.setError(getString(R.string.error_passwords_same));
            focusView = mPasswordView;
            mPasswordRepeatedView.setError(getString(R.string.error_passwords_same));
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;

        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if(TextUtils.isEmpty(displayName)){
            mDisplayName.setError(getString(R.string.error_field_required));
            focusView = mDisplayName;
            cancel = true;
        }

        if (TextUtils.isEmpty(lastName)) {
            mSurname.setError(getString(R.string.error_field_required));
            focusView = mSurname;
            cancel = true;
        }

        if(TextUtils.isEmpty(name)){
            mDisplayName.setError(getString(R.string.error_field_required));
            focusView = mDisplayName;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            showProgress(true);
            /*Connections conn = new Connections();
            String[] params = {displayName,email, password};

            conn.callToWS(Connections.CREATION, params,0);*/


            Session session = new SessionImpl(Utils.URL_BASE, new BasicAuthentication(Utils.DEFAULT_APK_ACCOUNT_EMAIL,Utils.DEFAULT_APK_ACCOUNT_PASS));
            UserService userService = new UserService(session);
            Callback callback = new JSONObjectCallback() {
                 @Override
                 public void onFailure(Exception exception) {
                     Log.d("SINGUP--->KO", exception.toString());
                     showProgress(false);
                     Snackbar.make(findViewById(R.id.singup_form), exception.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();


                 }

                 @Override
                 public void onSuccess(JSONObject result) {
                     //USer successfully created
                     Log.d("SINGUP--->OK", result.toString());
                     Utils.setUserInfoAfterLoginOrSingUp(result,dbhelper.getWritableDatabase(),password,getApplicationContext(),getIntent(),true);
                 }
             };

            try {
                session.setCallback(callback);
                //Log.d("TO-SEND",Utils.DEFAULT_COMPANY_ID+" "+password+" "+displayName+" "+email+" "+name+" "+lastName);
               userService.addUser(Utils.DEFAULT_COMPANY_ID,false,password,password,false,displayName,email,0,"", "",name,"",lastName,0,0,true,1,1,1970,"",null,null,null,null,false,null);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public static void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = ctx.getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<String>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }


    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(SingUpActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }



}

