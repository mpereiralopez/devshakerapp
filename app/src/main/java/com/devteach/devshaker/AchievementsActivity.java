package com.devteach.devshaker;


import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.devteach.devshaker.model.DevShackerDbHelper;
import com.devteach.devshaker.model.User;
import com.devteach.devshaker.utils.MenuActivityase;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by mpereira on 5/6/15.
 */
public class AchievementsActivity extends MenuActivityase {


    private DevShackerDbHelper dbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_achivements, contentFrameLayout);
        TextView userNameLabel = (TextView)findViewById(R.id.achivements_user_name);
        dbHelper = DevShackerDbHelper.getInstance(this);

        User  user = User.getUserFromDDBB(dbHelper.getReadableDatabase());

        userNameLabel.setText(user.getScreenName());

        CircleImageView circleImageView = (CircleImageView) findViewById(R.id.profile_circle_pic);

       // Utils.loadImageFromStorage(this,circleImageView);

    }


}
