package com.devteach.devshaker;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.devteach.devshaker.model.DevShackerDbHelper;
import com.devteach.devshaker.model.User;
import com.devteach.devshaker.utils.RegistrationService;

import java.util.Locale;
import java.util.UUID;

public class Splash extends Activity {

    private final int SPLASH_DURATION = 3000;
    private static SharedPreferences prefs;
    private DevShackerDbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        prefs =this.getSharedPreferences("com.devteach.devshaker", MODE_PRIVATE);
        dbHelper = DevShackerDbHelper.getInstance(this);
        String sDefSystemLanguage = Locale.getDefault().getLanguage();

        // TOKEN DATA GENERATION FOR SESSION

        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        uuid.substring(0,20);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("tokenData",uuid);
        editor.commit();
        // OJETE ESO ES PARA DEBUG
        //this.deleteDatabase(DevShackerDbHelper.DATABASE_NAME);
        // OJETE ESO ES PARA DEBUG
        Intent i = new Intent(this, RegistrationService.class);
        startService(i);

        new Handler().postDelayed(new Runnable() {

            public void run() {

                Log.d("prefs","prefs");
                if (prefs.getBoolean("firstrun", true)) {
                    // Do first run stuff here then set 'firstrun' as false
                    // using the following line to edit/commit prefs

                    Log.d("FirstRun", "True");
                    Intent mainIntent = new Intent(Splash.this, TutorialActivity.class);
                    Splash.this.startActivity(mainIntent);
                    Splash.this.finish();
                }else{
                    Log.d("FirstRun", "False");
                    Intent mainIntent = null;
                    if (User.getUserFromDDBB(dbHelper.getReadableDatabase())==null){
                        mainIntent = new Intent(Splash.this,TutorialActivity.class);
                    }else{
                        mainIntent = new Intent(Splash.this,LandingActivity.class);
                    }
                    Splash.this.startActivity(mainIntent);
                    Splash.this.finish();
                }



            }
        }, SPLASH_DURATION);
    }
}
