package com.devteach.devshaker;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.devteach.devshaker.model.Category;
import com.devteach.devshaker.model.DevShackerDbHelper;
import com.devteach.devshaker.model.Level;
import com.devteach.devshaker.model.Subcategory;
import com.devteach.devshaker.model.User;
import com.devteach.devshaker.utils.MenuActivityase;
import com.devteach.devshaker.utils.Utils;
import com.liferay.mobile.android.auth.basic.BasicAuthentication;
import com.liferay.mobile.android.callback.Callback;
import com.liferay.mobile.android.callback.typed.JSONArrayCallback;
import com.liferay.mobile.android.service.BatchSessionImpl;
import com.liferay.mobile.android.service.Session;
import com.liferay.mobile.android.service.SessionImpl;
import com.liferay.mobile.android.v7.api.ApiService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;


public class LandingActivity extends MenuActivityase {


    private static DevShackerDbHelper dbhelper;
    private View mProgressView;
    private View mLandingView;
    private List<Category> categoryList = new LinkedList<Category>();
    private static User user=null;
    private View ButtonCodeIMG;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbhelper = DevShackerDbHelper.getInstance(this);
        categoryList = Category.getCategoriesFromDDBB(dbhelper.getReadableDatabase());
        user = User.getUserFromDDBB(dbhelper.getReadableDatabase());
        //setContentView(R.layout.activity_landing);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.main_landing, contentFrameLayout);

        MenuActivityase.setToolbarColor("default");
        MenuActivityase.setActivityTitlebyResource(R.string.title_activity_landing);


        mLandingView = findViewById(R.id.landing_view);
        mProgressView = findViewById(R.id.landing_progress);
        ButtonCodeIMG = findViewById(R.id.button_code);

        //ButtonCodeIMG.animate().x(150).y(150).setDuration(2000).withLayer();

        if(categoryList.size()==0){
            showProgress(true);
            Bundle extras = getIntent().getExtras();

                user.setEmailAddress(Utils.DEFAULT_APK_ACCOUNT_EMAIL);
                user.setPassword(Utils.DEFAULT_APK_ACCOUNT_PASS);

            downloadCategories(user);
            String welcomeMSG ="Welcome back "+user.getScreenName();

            Snackbar.make(findViewById(R.id.drawer_layout), welcomeMSG, Snackbar.LENGTH_LONG).show();
        }



    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLandingView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLandingView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLandingView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLandingView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    private void downloadCategories(final User user){
        //No hay categorias.. hay que descargarlas
        Session sessionCategories = new SessionImpl(Utils.URL_BASE,new BasicAuthentication(user.getEmailAddress(),user.getPassword()));
        final ApiService devshakerService = new ApiService(sessionCategories);
        Callback callback = new JSONArrayCallback() {
            @Override
            public void onFailure(Exception exception) {
                Log.d("LANDING ",exception.toString());
            }

            @Override
            public void onSuccess(JSONArray result) {
                //List<Category>categoryListAux = new LinkedList<Category>();
                Log.d("LANDING ", "Rescatados "+ result.length()+" categorias");
                Category cat = null;
                JSONObject obj = null;
                for(int i=0;i<result.length();i++){
                    try {
                        obj = result.getJSONObject(i);
                        long catId = obj.getLong("categoryId");
                        String catName = obj.getString("nameCurrentValue");
                        cat = new Category(catId,catName);
                        Category.saveCategoryToDDBB(cat,dbhelper.getWritableDatabase());
                        categoryList.add(cat);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                downloadSubcategories(user, categoryList);
                //showProgress(false);
            }
        };

        sessionCategories.setCallback(callback);

        try {
            devshakerService.getCategoriesOfCompany();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    /**
     * Method return Subcategories of each Categorie
     * @param user
     * @param categoryList
     */

    private void downloadSubcategories(final User user, final List<Category> categoryList){
        Session sessionForSubcategories = new SessionImpl(Utils.URL_BASE,new BasicAuthentication(user.getEmailAddress(),user.getPassword()));
        BatchSessionImpl batch = new BatchSessionImpl(sessionForSubcategories);
        ApiService devshakerServiceSubcategories = new ApiService(batch);
        batch.setCallback(new JSONArrayCallback() {
            @Override
            public void onSuccess(JSONArray result) {
                List<Subcategory> subcategoryList = new LinkedList<Subcategory>();
                Log.d("SUBCATEGORIES-->OK",result.toString());
                for(int i =0; i<result.length();i++){
                    JSONObject obj = null;
                    Subcategory subcat = null;
                    try {
                        obj = result.getJSONObject(i);
                        long subcategoryId = obj.getLong("subcategoryId");
                        long categoryId = obj.getLong("categoryId");
                        String subcategoryName = obj.getString("subcategoryNameCurrentValue");
                        subcat = new Subcategory(subcategoryId,categoryId,subcategoryName);
                        subcategoryList.add(subcat);
                        Subcategory.saveSubcategoryToDDBB(subcat,dbhelper.getWritableDatabase());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                downloadLevelsOfSubcategory(user, subcategoryList);
            }

            @Override
            public void onFailure(Exception exception) {
                Log.d("SUBCATEGORIES-->KO",exception.toString());
            }
        });

        for(Category cat : categoryList) {
            try {
                devshakerServiceSubcategories.getSubCategoriesOfCategory(cat.getId());
                batch.invoke();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * This method return Levels from Server
     * @param user
     * @param subcategoryList
     */
    private void downloadLevelsOfSubcategory(User user, final List<Subcategory> subcategoryList) {

        Session sessionForLevel = new SessionImpl(Utils.URL_BASE,new BasicAuthentication(user.getEmailAddress(),user.getPassword()));
        BatchSessionImpl batch = new BatchSessionImpl(sessionForLevel);
        ApiService devshakerServiceLevels = new ApiService(batch);
        batch.setCallback(new JSONArrayCallback() {
            @Override
            public void onSuccess(JSONArray result) {
                //List<Subcategory> subcategoryList = new LinkedList<Subcategory>();
                Log.d("LEVEL-->OK",result.toString());

                for(int i =0; i<result.length();i++){
                    JSONObject obj = null;
                    Level level = null;
                    try {
                        obj = result.getJSONObject(i);
                        long levelId = obj.getLong("levelId");
                        long subcategoryId = obj.getLong("subcategoryId");
                        String levelName = obj.getString("levelNameCurrentValue");
                        level = new Level(levelId,levelName,subcategoryId,(i==0)?Level.LEVEL_STATUS_NOT_STARTED:Level.LEVEL_STATUS_BLOQUED,5,0);
                        Level.saveLevelsToDDBB(level, dbhelper.getWritableDatabase());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                    showProgress(false);


                //downloadLevelsOfSubcategory(user, subcategoryList);
            }

            @Override
            public void onFailure(Exception exception) {
                Log.d("LEVEL-->KO",exception.toString());
            }
        });

        for(Subcategory subcat : subcategoryList) {
            try {
                devshakerServiceLevels.getLevelOfSubcategory(subcat.getId());
                batch.invoke();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    public void goToCodeLanding(View view){
        //System.out.println("Button pressed");
        //Aqui lanzo las preguntas
        Intent mainIntent = new Intent(LandingActivity.this,SubcategoryActivity.class);
        mainIntent.putExtra("catId", categoryList.get(0).getId());
        LandingActivity.this.startActivity(mainIntent);
        //LoginActivity.this.finish();
    }

    public void goToBuildLanding(View view){
        //System.out.println("Button pressed");
        //Aqui lanzo las preguntas
        Intent mainIntent = new Intent(LandingActivity.this,SubcategoryActivity.class);
        mainIntent.putExtra("catId", categoryList.get(1).getId());
        LandingActivity.this.startActivity(mainIntent);
        //LoginActivity.this.finish();
    }

    public void goToThinkLanding(View view){
        //System.out.println("Button pressed");
        //Aqui lanzo las preguntas
        Intent mainIntent = new Intent(LandingActivity.this,SubcategoryActivity.class);
        mainIntent.putExtra("catId", categoryList.get(2).getId());
        LandingActivity.this.startActivity(mainIntent);
        //LoginActivity.this.finish();
    }


}
