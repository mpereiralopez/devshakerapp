package com.devteach.devshaker;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.devteach.devshaker.model.Badge;
import com.devteach.devshaker.model.Category;
import com.devteach.devshaker.model.DevShackerDbHelper;
import com.devteach.devshaker.model.Level;
import com.devteach.devshaker.model.Subcategory;
import com.devteach.devshaker.model.User;
import com.devteach.devshaker.utils.ExperienceCalculator;
import com.devteach.devshaker.utils.MenuActivityase;
import com.devteach.devshaker.utils.Utils;
import com.liferay.mobile.android.auth.basic.BasicAuthentication;
import com.liferay.mobile.android.callback.Callback;
import com.liferay.mobile.android.callback.typed.JSONObjectCallback;
import com.liferay.mobile.android.service.Session;
import com.liferay.mobile.android.service.SessionImpl;
import com.liferay.mobile.android.v7.api.ApiService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by mpereira on 11/4/15.
 */
public class LevelCompleted extends MenuActivityase {

    private Activity act;
    private TextView percentageComplete;
    private LinearLayout responseStatusContainer;
    private boolean isFromTutorial = false;
    private Subcategory subcategory;
    private Level level;
    private static DevShackerDbHelper dbhelper;
    private int percentage;
    private Context ctx;
    private static SharedPreferences prefs;

    private ProgressBar progressBar;
    private TextView progressNumber;
    private TextView xpObtainedTextView;
    private static String categoryName = "Tutorial";
    private String arrayResponsesStr = new String();
    private static User u;
    private static int xpObtained = 0;
    private static String levelOfUser = new String();

    //LevelCompleted levelCompleted = new LevelCompleted(int percentageComplete, );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.level_complete, contentFrameLayout);
        //setContentView(R.layout.level_complete);
        prefs = this.getSharedPreferences("com.devteach.devshaker", MODE_PRIVATE);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setVisibility(View.GONE);
        ctx = this;
        act = this;
        int percentCorrect = 0;
        int aux = 0;
        //lvlCompletedHeader = (TextView) findViewById(R.id.lvlCompletedHeader);
        TextView lvlcompleted = (TextView) findViewById(R.id.lvlcompleted);
        progressBar = (ProgressBar) findViewById(R.id.progressBar2);
        progressNumber = (TextView) findViewById(R.id.progressNumber);
        xpObtainedTextView = (TextView) findViewById(R.id.xpobtained);
        Bundle extras = getIntent().getExtras();
        ImageView cerclePunctuation = (ImageView) findViewById(R.id.punctuation_cercle_level_completed);
        int userXp = 0;
        dbhelper = DevShackerDbHelper.getInstance(this);
        List <Boolean> listaRespuestasTest = new LinkedList<>();

        if (extras != null) {
            boolean[] list_answerses = extras.getBooleanArray("LIST_ANSWERS");
            for (int i = 0; i < list_answerses.length; i++) {
                listaRespuestasTest.add(list_answerses[i]);
                if (list_answerses[i] == true) aux = aux + 1;
            }
            percentCorrect = (aux * 100) / listaRespuestasTest.size();
            isFromTutorial = extras.getBoolean("isFromTutorial");
            //lvlCompletedHeader.setText(extras.getString("LevelCompletedHeader"));
            String lvl = "";
            level = (Level) getIntent().getSerializableExtra("Level");
            lvlcompleted.setText("Level " + lvl + " completed");
            subcategory = (Subcategory) getIntent().getSerializableExtra("subcategory");
            if (isFromTutorial) {
                lvl = "Tutorial";
                MenuActivityase.setActivityTitlebyResource(R.string.title_activity_tutorial);

                cerclePunctuation.setBackgroundResource(R.drawable.level_complete_tutorial_circle);
                android.support.v7.widget.Toolbar mToolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
                mToolbar.setNavigationIcon(null);
                mToolbar.setNavigationOnClickListener(null);
            } else {
                lvl = extras.getString("lvlcompleted");
                u = User.getUserFromDDBB(dbhelper.getReadableDatabase());
                userXp = u.getXp();
                categoryName = Category.getCategoryFromDDBB(dbhelper.getReadableDatabase(), subcategory.getCategory()).getName();
                if (categoryName.equalsIgnoreCase("code")) {
                    cerclePunctuation.setBackgroundResource(R.drawable.level_complete_code_circle);
                }
                if (categoryName.equalsIgnoreCase("build")) {
                    cerclePunctuation.setBackgroundResource(R.drawable.level_complete_build_circle);
                }
                if (categoryName.equalsIgnoreCase("think")) {
                    cerclePunctuation.setBackgroundResource(R.drawable.level_complete_think_circle);
                }
                MenuActivityase.setToolbarColor(categoryName);
                MenuActivityase.setActivityTitlebyText(subcategory.getName() + " " + level.getName());

            }
            arrayResponsesStr = extras.getString("arrayResponses");


        }

        percentageComplete = (TextView) findViewById(R.id.percentageComplete);
        responseStatusContainer = (LinearLayout) findViewById(R.id.responseStatusContainer);
        Iterator<Boolean> it = listaRespuestasTest.iterator();
        int index = 0;
        while (it.hasNext()) {
            index++;
            boolean status = it.next();
            HashMap<String, Object> map = new HashMap<>();
            map.put(Utils.LEVEL_COMMPLETED_KEY, status);
            map.put("index", index);
            //map.put("userAnswer",);
            //map.put("correctAnswer",);
            TextView textView = Utils.createResponseCircle(getApplicationContext(), getResources(), map, Integer.toString(index), Utils.answerStatusRadious);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent mainIntent = new Intent(LevelCompleted.this, QuestionTestDetailActivity.class);
                    int idOfView = view.getId();
                    mainIntent.putExtra("userResponse", arrayResponsesStr);
                    mainIntent.putExtra("index", idOfView - 1);
                    mainIntent.putExtra("categoryName", categoryName);
                    mainIntent.putExtra("IS_FROM_TUTORIAL", isFromTutorial);
                    LevelCompleted.this.startActivity(mainIntent);
                }
            });
            responseStatusContainer.addView(textView);
        }

        this.percentage = percentCorrect;
        percentageComplete.setText(percentCorrect + "%");

        /** Mando datos al servidor para que me devuelva el progreso. */
        if (Utils.haveNetworkConnection(this.ctx) && !isFromTutorial) {
            Session session = new SessionImpl(Utils.URL_BASE, new BasicAuthentication(u.getEmailAddress(), u.getPassword()));
            ApiService devshakerService = new ApiService(session);
            Callback callback = new JSONObjectCallback() {
                @Override
                public void onFailure(Exception exception) {
                    Log.d("LevelComplete", exception.toString());
                }

                @Override
                public void onSuccess(JSONObject jsonObject) {
                    Log.d("LevelComplete" ,"RESULTS "+ jsonObject);
                    try {
                        xpObtained = jsonObject.getInt("userXp");
                        levelOfUser = jsonObject.getString("userLevel");
                        int prevUserXp = u.getXp();
                        System.out.println("-------> " + prevUserXp);
                        User.updateUserXp(u.getUserId(), jsonObject.getInt("userXp"), dbhelper.getReadableDatabase());
                        xpObtainedTextView.setText("+" + (xpObtained - prevUserXp) + " XP");
                        int[] values = ExperienceCalculator.getLevelOfProgressionOfUser(xpObtained);

                        progressNumber.setText(levelOfUser);
                        int progressForBar = (xpObtained * 100) / values[1];


                        progressBar.setProgress(progressForBar);

                        // Now check if badges or achievements arrive
                        boolean showPopUp = false;
                        JSONArray badgesArray = jsonObject.getJSONArray("badges");
                        Badge badge = null;
                        List<Badge> listaBadges = new LinkedList<Badge>();
                        for (int i = 0; i < badgesArray.length(); i++) {
                            showPopUp = true;
                            JSONObject badgeJSON = badgesArray.getJSONObject(i);
                            //Long ID, String name, String description, String img, long date
                            long id = badgeJSON.getLong("id");
                            String title = badgeJSON.getString("title");

                            String desc = badgeJSON.getString("description");
                            long date = badgeJSON.getLong("date");
                            String imageUrl = badgeJSON.getString("img").substring(1);
                            if(imageUrl.indexOf("http")==-1){
                                imageUrl = Utils.URL_BASE+"/"+imageUrl;
                            }
                            badge = new Badge(id, title, desc, imageUrl, date,badgeJSON.getString("subCategoryName"),badgeJSON.getString("categoryName"));
                            listaBadges.add(badge);
                            //Descargo imagen y guardo en BBDD en segundo plano
                            Utils.downloadImageFromServer(imageUrl, dbhelper.getWritableDatabase(), badge, ctx);
                        }
                        if (showPopUp) {
                            Utils.showSortPopup(act, listaBadges);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            };
            session.setCallback(callback);
            try {
                devshakerService.sendTestResultOfUser(arrayResponsesStr);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void goToMenu(View view) {
        if (isFromTutorial) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("firstrun", false);
            editor.commit();
            Intent mainIntent = new Intent(LevelCompleted.this, LoginActivity.class);
            LevelCompleted.this.startActivity(mainIntent);
            LevelCompleted.this.finish();

        } else {
            Log.d("LevelCompleted", level.getID() + "");
            Level.updateLevelStatusAndPercentage(level, this.percentage, dbhelper.getReadableDatabase());
            Intent mainIntent = new Intent(LevelCompleted.this, LevelActivity.class);
            mainIntent.putExtra("Subcategory", subcategory);
            LevelCompleted.this.startActivity(mainIntent);
            LevelCompleted.this.finish();
        }


    }

    public void shareToSocial(View view) {
        //Snackbar.make(findViewById(R.id.drawer_layout), "This is a beta version. Feature available soon", Snackbar.LENGTH_LONG).show();

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "\n\n");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Comparto desde ");
        startActivity(Intent.createChooser(sharingIntent,  "Para compartir"));
    }


    public void shareWithFriends(View view) {
        Snackbar.make(findViewById(R.id.drawer_layout), "This is a beta version. Feature available soon", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Log.d("LEVEL_COMPLETE", "DONT LET YOU GO ANYWAY");
    }




}
