package com.devteach.devshaker;


import android.content.Intent;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.devteach.devshaker.model.Category;
import com.devteach.devshaker.model.DevShackerDbHelper;
import com.devteach.devshaker.model.Level;
import com.devteach.devshaker.model.Subcategory;
import com.devteach.devshaker.utils.MenuActivityase;

import java.util.List;

/**
 * Created by mpereira on 5/6/15.
 */
public class LevelActivity extends MenuActivityase implements View.OnClickListener {

    private Subcategory subcategory;
    private Category category;
    private List<Level> levels;
    private static  DevShackerDbHelper dbhelper;
    private Level levelClickable;
    private int totalLevels;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //GET PARAMETERS
        subcategory =(Subcategory)getIntent().getSerializableExtra("Subcategory");


        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_levels, contentFrameLayout);


        dbhelper = DevShackerDbHelper.getInstance(this);

        category = Category.getCategoryFromDDBB(dbhelper.getReadableDatabase(),subcategory.getCategory());

        levels = Level.getLevelsOfSubcategory(subcategory.getId(), dbhelper.getReadableDatabase());
        totalLevels = levels.size();
        String categoryName= Category.getCategoryFromDDBB(dbhelper.getReadableDatabase(),subcategory.getCategory()).getName();

        MenuActivityase.setToolbarColor(categoryName);
        MenuActivityase.setActivityTitlebyText(subcategory.getName());


        TableRow row=null;
        TableLayout table = (TableLayout)findViewById(R.id.levels_table);
        Button btn = null;
        TextView txt = null;
        for(int i=0; i<levels.size();i++){

            row= (TableRow)table.getChildAt(i);
            row.setVisibility(View.VISIBLE);

            btn = (Button)row.getChildAt(0);
            txt = (TextView)row.getChildAt(1);
            btn.setText(String.format("%02d", i+1));

            String  textToPut = new String();
            Level level = levels.get(i);

            System.out.println("---------------------------------- "+level.getStatus());

            if(level.getStatus().equalsIgnoreCase(Level.LEVEL_STATUS_COMPLETED) ){
                int drawableResource = -1;
                if(categoryName.equalsIgnoreCase("code")){ drawableResource =  R.drawable.lvcomp_code;}
                if(categoryName.equalsIgnoreCase("build")){ drawableResource = R.drawable.lvcomp_build;}
                if(categoryName.equalsIgnoreCase("think")){ drawableResource = R.drawable.lvcomp_think;}

                btn.setBackgroundDrawable(getResources().getDrawable(drawableResource));
                textToPut = "100% complete\n"+level.getPercentage()+"% correct";
                btn.setTextColor(Color.WHITE);
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //GO TO LEVEL COMPLETE
                        /*Intent mainIntent = new Intent(LevelActivity.this, LevelCompleted.class);
                        //mainIntent.putExtra("LIST_ANSWERS", listArray);
                        mainIntent.putExtra("isFromTutorial", false);
                        //mainIntent.putExtra("arrayResponses", this.responses.toString());
                        String header = subcategory.getName() + " - Level " + level.getName();
                        mainIntent.putExtra("LevelCompletedHeader", header);
                        mainIntent.putExtra("lvlcompleted", level.getName());
                        mainIntent.putExtra("subcategory", subcategory);
                        mainIntent.putExtra("Level", level);
                        LevelActivity.this.startActivity(mainIntent);*/

                        Snackbar.make(findViewById(R.id.drawer_layout), "Level completed. Choose next", Snackbar.LENGTH_LONG).show();

                    }
                });
            }
            if(level.getStatus().equalsIgnoreCase(Level.LEVEL_STATUS_NOT_STARTED)){
                int drawableResource = -1;
                int colorForText = -1;
                if(categoryName.equalsIgnoreCase("code")){ drawableResource =  R.drawable.lvincomp_code;colorForText=R.color.color_code;}
                if(categoryName.equalsIgnoreCase("build")){ drawableResource = R.drawable.lvincomp_build;colorForText=R.color.color_build;}
                if(categoryName.equalsIgnoreCase("think")){ drawableResource = R.drawable.lvincomp_think;colorForText=R.color.color_think;}

                btn.setBackgroundDrawable(getResources().getDrawable(drawableResource));
                btn.setTextColor(getResources().getColor(colorForText));
                levelClickable = level;
                btn.setOnClickListener(this);
                textToPut="not started";
            }

            if(level.getStatus().equalsIgnoreCase(Level.LEVEL_STATUS_BLOQUED)){
                int drawableResource = -1;
                int colorForText = -1;
                if(categoryName.equalsIgnoreCase("code")){ drawableResource =  R.drawable.lvbloc_code;colorForText=R.color.code_block;}
                if(categoryName.equalsIgnoreCase("build")){ drawableResource = R.drawable.lvbloc_build;colorForText=R.color.build_block;}
                if(categoryName.equalsIgnoreCase("think")){ drawableResource = R.drawable.lvbloc_think;colorForText=R.color.think_block;}
                btn.setBackgroundDrawable(getResources().getDrawable(drawableResource));
                btn.setTextColor(getResources().getColor(colorForText));
                textToPut="blocked";
            }
            //textToPut+="\n" totalLevels.get(i).getPercentage()+"% correct"
            txt.setText(textToPut);
        }
    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        Intent mainIntent = new Intent(LevelActivity.this,QuestionTestActivity.class);
        mainIntent.putExtra("Subcategory",subcategory);
        mainIntent.putExtra("Level",levelClickable);
        mainIntent.putExtra("totalLevels",totalLevels);
        mainIntent.putExtra("IS_FROM_TUTORIAL",false);
        LevelActivity.this.startActivity(mainIntent);

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Log.d("LEVEL_ACT","DONT LET YOU GO ANYWAY");
        Intent mainIntent = new Intent(LevelActivity.this,SubcategoryActivity.class);
        mainIntent.putExtra("catId", category.getId());
        LevelActivity.this.startActivity(mainIntent);
    }

}
